package com.deliver.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.deliver.DeliverApplication;
import com.deliver.R;
import com.deliver.customobjects.Address;
import com.deliver.customobjects.DeliverCard;
import com.deliver.customobjects.Quotation;
import com.deliver.customobjects.responses.ClientTrackingResponse;
import com.deliver.customobjects.responses.CreateServiceResponse;
import com.deliver.customobjects.responses.GeoLocationResult;
import com.deliver.customobjects.PricingState;
import com.deliver.customobjects.ProviderMapObject;
import com.deliver.customobjects.responses.GettingServiceResponse;
import com.deliver.customobjects.responses.ServiceExistsResponse;
import com.deliver.utils.Constants;
import com.deliver.utils.DistanceMatrix;
import com.deliver.customobjects.requests.QuotationCharacteristics;
import com.deliver.utils.LatLngInterpolator;
import com.deliver.utils.MarkerAnimation;
import com.deliver.utils.Utils;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MapsActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener, GoogleMap.OnCameraChangeListener, View.OnClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, RoutingListener {

    private static final int PERMISSIONS = 100;
    private static final int REGISTER_CARD = 101;
    private GoogleMap mMap;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private boolean firstTime = true;
    private boolean userInteraction;
//    Geocoder geoCoder =
    private TextView addressTextView;
    private Button confirmStartButton;

    private Address startAddress;
    private Address destinationAddress;

    private android.location.Address addrStart;
    private android.location.Address addrDestination;

    private TextView destinationAddressTextView;
    private Button confirmDestinationButton;

    private Button proceedWithPricingButton;

    PricingState pricingState = PricingState.StartLocation;
    private RelativeLayout destinationLocationView;
    private GeoLocationResult lastLocationResult;
    private Marker startMarker;
    private Marker destinationMarker;

    private QuotationCharacteristics characteristics;
    private DistanceMatrix distance;
    private String calculatedAmount;
    private TextView quotationDistanceTextView;
    private TextView quotationTripDurationTextView;
    private TextView quotationCostTextView;
    private LinearLayout quotationInfoView;
    private RelativeLayout startLocationView;
    private static final int START_LOCATION = 100;
    private static final int DESTINATION_LOCATION = 200;

    Gson gson = new Gson();
    private Boolean isProviderEnabled;
    private LinearLayout pickPlacesLayout;
    private RelativeLayout pinLayout;

    GetProviders checkProviders = new GetProviders();
    ServiceExists serviceExists = new ServiceExists();
    ProviderServiceStatus serviceStatus = new ProviderServiceStatus();
    GettingService gettingServiceRunnable = new GettingService();

    ProviderServiceTracking serviceTracking = new ProviderServiceTracking();
    ClientServiceTracking clientServiceTracking = new ClientServiceTracking();
    long m_interval = 15000;
    private boolean updateProviders = true;
    private Location myLocation;
    private Handler m_handler = new Handler();
    private Geocoder geocoder;
    private android.location.Address lastAddressResult;
    private HashMap<String, Marker> mMarkers = new HashMap<>();
    private LatLngInterpolator interpolator = new LatLngInterpolator.Spherical();
    private Marker mCurrentMarker;
    private Quotation myCurrentQuotation;
    private List<ProviderMapObject> mAllProviders = new ArrayList<>();
    private TextView quotationProvider;
    private Long currentProviderRequestId; //For handshake between client and provider
    private Long currentServiceId; //For when the service has been confirmed
    private ProgressDialog loadingDialog;
    private ArrayList<Polyline> polylines = new ArrayList<>();
//    private Button myLocationButton;
    private ServiceStatus currentServiceStatus = ServiceStatus.NoService;;
    private int providerServiceStatus = 0;

    private boolean isUserProvider = DeliverApplication.getInstance().isUserProvider();

    private Location fromMyLocation;
    private LatLng providerStartLocation;
    private LatLng providerDestinationLocation;
    private boolean shouldStartGettingProvider = true;
    private Button beginTripButton;
    private Route currentRoute;
    private Marker trackingProviderMarker;

    LinearLayout ratingContainer;
    RatingBar ratingBarAttention;
    RatingBar ratingBarQuality;
    RatingBar ratingBarTime;
    private EditText editTextComments;
    private Button buttonSendRating;
    private boolean shouldTakeSnapShot;


    public void setCurrentServiceId(long currentServiceId) {
        this.currentServiceId = currentServiceId;
        DeliverApplication.getInstance().setCurrentServiceId(currentServiceId);
    }

    public void setCurrentServiceStatus(ServiceStatus currentServiceStatus) {
        this.currentServiceStatus = currentServiceStatus;
        if (isUserProvider) {

        } else {
            if (currentServiceStatus == ServiceStatus.NoService) {
                pinLayout.setVisibility(View.VISIBLE);
            } else {
                pinLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

    enum ServiceStatus {
        NoService(0),
        OnItsWayToOrigin(1),
        CollectingPackage(2),
        InTransitToDestination(3),
        UnloadingPayload(4),
        DeliveryFinished(5),
        Canceled(6),
        Paused(7);

        private int code;

        ServiceStatus(int code){
            this.code = code;
        }

        public static ServiceStatus serviceFromCode(int Code) {
            switch(Code) {
                case 1:
                    return OnItsWayToOrigin;
                case 2:
                    return CollectingPackage;
                case 3:
                    return InTransitToDestination;
                case 4:
                    return UnloadingPayload;
                case 5:
                    return DeliveryFinished;
                case 6:
                    return Canceled;
                case 7:
                    return Paused;
            }
            return NoService;
        }

        public int getCode() {
            return code;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        addressTextView = (TextView)findViewById(R.id.addressTextView);
        confirmStartButton = (Button)findViewById(R.id.confirmStartButton);
        confirmStartButton.setOnClickListener(this);

        destinationAddressTextView = (TextView)findViewById(R.id.destinationAddressTextView);
        confirmDestinationButton = (Button)findViewById(R.id.confirmDestinationButton);
        confirmDestinationButton.setOnClickListener(this);

        proceedWithPricingButton = (Button)findViewById(R.id.proceedWithPricingButton);
        proceedWithPricingButton.setOnClickListener(this);

        startLocationView = (RelativeLayout)findViewById(R.id.startLocationView);
        startLocationView.setOnClickListener(this);
        destinationLocationView = (RelativeLayout)findViewById(R.id.destinationLocationView);
        destinationLocationView.setOnClickListener(this);

        pickPlacesLayout = (LinearLayout)findViewById(R.id.pickPlacesLayout);

        quotationInfoView = (LinearLayout)findViewById(R.id.quotationInfoView);
        quotationDistanceTextView = (TextView)findViewById(R.id.quotationDistanceTextView);
        quotationTripDurationTextView = (TextView)findViewById(R.id.quotationTripDurationTextView);
        quotationCostTextView = (TextView)findViewById(R.id.quotationCostTextView);
        quotationProvider = (TextView) findViewById(R.id.quotationProvider);

        pinLayout = (RelativeLayout)findViewById(R.id.pinLayout);

//        myLocationButton = (Button) findViewById(R.id.myLocationButton);
//        myLocationButton.setOnClickListener(this);

        beginTripButton = (Button) findViewById(R.id.beginTripButton);
        beginTripButton.setOnClickListener(this);

        ratingContainer =(LinearLayout)findViewById(R.id.ratingContainer);
        ratingBarAttention = (RatingBar)findViewById(R.id.ratingBarAttention);
        ratingBarQuality = (RatingBar)findViewById(R.id.ratingBarQuality);
        ratingBarTime = (RatingBar)findViewById(R.id.ratingBarTime);
        editTextComments = (EditText)findViewById(R.id.editTextComments);
        buttonSendRating = (Button)findViewById(R.id.buttonSendRating);
        buttonSendRating.setOnClickListener(this);


        isProviderEnabled = getIntent().getBooleanExtra("provider", false);
        if (isProviderEnabled) {
            setPricingState(PricingState.NoState);
            pickPlacesLayout.setVisibility(View.GONE);
            pinLayout.setVisibility(View.GONE);
        } else {
            characteristics = getIntent().getParcelableExtra("QuotationCharacteristics");
        }
        Locale locale = new Locale(Locale.getDefault().getLanguage(), "MX");
        geocoder = new Geocoder(this, locale);

        setTitle("");
//        hideTopBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (menu != null) {
            MenuItem itemIncident = menu.findItem(R.id.action_incidence);
            if (!isUserProvider || (isUserProvider && currentProviderRequestId == null)) {
                itemIncident.setVisible(false);
            } else {
                itemIncident.setVisible(true);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_my_location:
            {
                goToMyLocation();
                return true;
            }
            case R.id.action_user_info:
            {
                Intent intent = new Intent();
                intent.setClass(this, CambiarDatosUsuario.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_incidence:
            {
                shouldTakeSnapShot = true;
                goToMyLocation();
//                mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
//                    @Override
//                    public void onSnapshotReady(Bitmap bitmap) {
//                        Intent intent = new Intent();
//                        intent.setClass(MapsActivity.this, CrearIncidencia.class);
//                        ByteArrayOutputStream out = new ByteArrayOutputStream();
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
//                        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
//                        intent.putExtra("MapSnapShot", decoded);
//                        startActivity(intent);
//                    }
//                });
            }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setBuildingsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS);
            return;
        }
        mMap.setOnMyLocationChangeListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);
//        new Geocoder(this, Locale.getDefault());

        // Add a marker in Sydney and move the camera

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        mMap.setOnMyLocationChangeListener(this);
                        mMap.setMyLocationEnabled(true);
                        mMap.setOnCameraChangeListener(this);
                    }catch (SecurityException e) {

                    }
                }
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isUserProvider) {
            startServiceExists();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        stopGettingProviders();

    }

    @Override
    public void onMyLocationChange(Location location) {

        if (currentServiceStatus == ServiceStatus.NoService) {
            myLocation = location;
            if (firstTime) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17.0f);
                mMap.animateCamera(cameraUpdate, 1, null);

            }
            if (isUserProvider) {
                DeliverApplication.getInstance().updateMyLocation(location, DeliverApplication.getInstance().getProviderId());
                startServiceExists();
            } else {
                if (shouldStartGettingProvider && !isUserProvider) {
                    shouldStartGettingProvider = false;
                    startGettingProviders();
                }
            }
            firstTime = false;
        } else {
            CameraUpdate cameraUpdate;
            if (isUserProvider) {
                myLocation = location;
                switch (currentServiceStatus) {
                    case OnItsWayToOrigin:
                        if (distanceBetween(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), providerStartLocation) < 50)
                        {
                            showServiceStateButton(R.string.text_begin_collecting_package);

                        }
                        break;
                    case CollectingPackage:
                    {

                    }
                        break;
                    case InTransitToDestination:
                    {
                        if (distanceBetween(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), providerDestinationLocation) < 50)
                        {
                            showServiceStateButton(R.string.text_begin_unloading_package);

                        }
                    }
                        break;
                }
            } else {
                myLocation = location;
                switch (currentServiceStatus) {
                    case OnItsWayToOrigin:
                        cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 15);
                        mMap.animateCamera(cameraUpdate);
                        break;
                    case CollectingPackage:
                        break;
                    case InTransitToDestination:
//                        cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 15);
//                        mMap.animateCamera(cameraUpdate);
                        break;
                    case UnloadingPayload:
                        break;
                }
            }
        }
    }

    private void showServiceStateButton(int textResource) {
        beginTripButton.setText(textResource);
        beginTripButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (userInteraction && !isUserProvider && pricingState == PricingState.StartLocation || pricingState == PricingState.DestinationLocation) {
            if (Constants.USE_NATIVE_GEOCODER) {
                new GetAddressFromLocationAsyncTask().execute(cameraPosition.target);
            } else {
                String url = Constants.URL_REVERSE_GEOCODE;
                url = url.replace("<lat>", "" + cameraPosition.target.latitude);
                url = url.replace("<long>", "" + cameraPosition.target.longitude);
                Ion.with(this).load(url)
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                if (e == null) {
                                    lastLocationResult = gson.fromJson(result.getResult(), GeoLocationResult.class);
                                    if (lastLocationResult != null) {
                                        if (lastLocationResult.status.equals("OK") && lastLocationResult.results.size() != 0) {
                                            Address address = lastLocationResult.results.get(0);
                                            if (pricingState == PricingState.StartLocation) {
                                                addressTextView.setText(address.formatted_address);
                                            } else {
                                                destinationAddressTextView.setText(address.formatted_address);
                                            }
                                        }
                                    }
                                } else {
                                    e.printStackTrace();
                                }

                            }
                        });
            }
        } else if (shouldTakeSnapShot) {
            shouldTakeSnapShot = false;
            mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                @Override
                public void onSnapshotReady(Bitmap bitmap) {
                    Intent intent = new Intent();
                    intent.setClass(MapsActivity.this, CrearIncidencia.class);
                    int width = bitmap.getWidth();
                    int height = (int)(width * 0.6);

                    Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0,(bitmap.getHeight() - height) / 2,width, height);
                    CrearIncidencia.sBitmap = resizedBitmap;
                    LatLng location = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    intent.putExtra("location", location);
                    intent.putExtra("serviceRequestId", currentServiceId);
                    startActivity(intent);
                }
            });
        }
        userInteraction = false;
    }

    private void showQuotationInfo() {
        setPricingState(PricingState.ShowingPricing);
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        userInteraction = true;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.confirmStartButton:
            {
                if (Constants.USE_NATIVE_GEOCODER) {
                    if (lastAddressResult == null) return;
                    setStartAddress(lastAddressResult);
                } else {
                    if (lastLocationResult == null) return;
                    setStartAddress(lastLocationResult.results.get(0));
                }
                setPricingState(PricingState.DestinationLocation);
            }
            break;
            case R.id.confirmDestinationButton:
            {
                if (Constants.USE_NATIVE_GEOCODER) {
                    if (lastAddressResult == null) return;
                    setDestinationAddress(lastAddressResult);
                } else {
                    if (lastLocationResult == null) return;
                    setDestinationAddress(lastLocationResult.results.get(0));
                }
                setPricingState(PricingState.Pricing);
            }
            break;
            case R.id.proceedWithPricingButton:
            {
                if (pricingState == PricingState.ShowingPricing) {
                    DeliverCard cards = DeliverApplication.getInstance().getCurrentCard();
                    if (cards == null) {
                        new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Deliver App")
                                .setContentText("Necesitas registrar una tarjeta para poder pedir un servicio")
                                .setConfirmText("Registrar")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent intent = new Intent();
                                        intent.setClass(MapsActivity.this, RegistroFormaPagoTDTC.class);
                                        startActivityForResult(intent, REGISTER_CARD);
                                    }
                                })
                                .show();
                    } else {
                        createServiceRequest();
                    }

                } else {
                    getDistanceBetweenLocations();
                }
            }
            break;
            case R.id.startLocationView:
            case R.id.destinationLocationView:
            {
                Intent intent = new Intent();
                intent.setClass(this, AddressSearchActivity.class);
                startActivityForResult(intent, v.getId() == R.id.startLocationView ? START_LOCATION : DESTINATION_LOCATION);
                setLeftEnterAnimation();
            }
            break;
            case R.id.myLocationButton:
            {
                goToMyLocation();
            }
            break;
            case R.id.beginTripButton:
            {
                if (currentServiceStatus == ServiceStatus.OnItsWayToOrigin) {
                    showServiceStateButton(R.string.text_begin_delivery);
                    setCurrentServiceStatus(ServiceStatus.CollectingPackage);
                } else if (currentServiceStatus == ServiceStatus.CollectingPackage) {
                    beginTripButton.setVisibility(View.GONE);
                    if (currentRoute != null) {
                        showRouteOnMap(currentRoute, 0xffd60000, true);
                        setCurrentServiceStatus(ServiceStatus.InTransitToDestination);
                    }
                } else if (currentServiceStatus == ServiceStatus.InTransitToDestination){
                    showServiceStateButton(R.string.text_finish_delivery);
                    setCurrentServiceStatus(ServiceStatus.UnloadingPayload);
                } else if (currentServiceStatus == ServiceStatus.UnloadingPayload) {
                    setCurrentServiceStatus(ServiceStatus.DeliveryFinished);//Finalizar entrega

                }
            }
            break;
            case R.id.buttonSendRating:
            {
                sendRate();
            }
                break;
        }
        userInteraction = false;
    }

    private void sendRate() {
        float ratingAttention = ratingBarAttention.getRating();
        float ratingQuality = ratingBarQuality.getRating();
        float ratingTime = ratingBarTime.getRating();
        String comments = editTextComments.getText().toString();

        JsonObject object = new JsonObject();
        object.addProperty("serviceRequestId", currentServiceId);
//        object.addProperty("id", currentServiceId);
        object.addProperty("time",(byte)ratingTime);
        object.addProperty("quality",(byte)ratingQuality);
        object.addProperty("attention",(byte)ratingAttention);
        if (comments != null && comments.length() != 0) {
            object.addProperty("comment", comments);
        }

        Ion.with(MapsActivity.this).load(Constants.URL_CLIENT_SERVICE_EVALUATION)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .setStringBody(object.toString())
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {

                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null && result.getHeaders().code() == 200) {//success
                            resetClientState();
                        } else if (e != null) {
                            new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Deliver App")
                                    .setContentText("Exception: " + e.getLocalizedMessage())
                                    .show();
                        } else if (result.getException() != null){
                            new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Deliver App")
                                    .setContentText("Exception: " + result.getException().getLocalizedMessage())
                                    .show();
                        } else {
                            new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Deliver App")
                                    .setContentText(result.getResult())
                                    .show();
                        }
                    }
                });
    }



    private void goToMyLocation() {
        if (mMap != null && myLocation != null) {
            LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            mMap.animateCamera(cameraUpdate);
        }
    }

    public void setStartAddress(Address address) {
        if (startMarker != null) {
            startMarker.remove();
            removeRouteFromMapIfPresent();
        }
        startAddress = address;
        LatLng location = startAddress.geometry.location.getLatLng();
        MarkerOptions marker = new MarkerOptions();
        marker.position(location);
        marker.title(startAddress.formatted_address);
        marker.icon(getBitmapDescriptor(true));
        startMarker = mMap.addMarker(marker);
    }

    public void setStartAddress(android.location.Address address) {
        if (startMarker != null) {
            startMarker.remove();
            removeRouteFromMapIfPresent();
        }
        addrStart = address;
        LatLng location = new LatLng(addrStart.getLatitude(), addrStart.getLongitude());
        MarkerOptions marker = new MarkerOptions();
        marker.position(location);
        marker.title(Utils.getFormattedAddress(addrStart));
        marker.icon(getBitmapDescriptor(true));
        startMarker = mMap.addMarker(marker);
    }

    private void setDestinationAddress(Address address) {
        if (destinationMarker != null) {
            destinationMarker.remove();
            removeRouteFromMapIfPresent();
        }
        destinationAddress = address;
        LatLng location = destinationAddress.geometry.location.getLatLng();
        MarkerOptions marker = new MarkerOptions();
        marker.position(location);
        marker.title(destinationAddress.formatted_address);
        marker.icon(getBitmapDescriptor(false));
        destinationMarker = mMap.addMarker(marker);

        LatLng start = startAddress.geometry.location.getLatLng();
        LatLng end = location;

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(start, end)
                .build();
        routing.execute();
    }

    private void setDestinationAddress(android.location.Address address) {
        if (destinationMarker != null) {
            destinationMarker.remove();
            removeRouteFromMapIfPresent();
        }
        addrDestination = address;
        LatLng location = new LatLng(addrDestination.getLatitude(), addrDestination.getLongitude());
        MarkerOptions marker = new MarkerOptions();
        marker.position(location);
        marker.title(Utils.getFormattedAddress(addrDestination));
        marker.icon(getBitmapDescriptor(false));
        destinationMarker = mMap.addMarker(marker);

        LatLng start = new LatLng(addrStart.getLatitude(), addrStart.getLongitude());
        LatLng end = location;

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(start, end)
                .build();
        routing.execute();
    }

    private BitmapDescriptor getBitmapDescriptor(boolean origin) {
        if (origin)
            return BitmapDescriptorFactory.fromResource(R.drawable.marker_origin);
        else
            return BitmapDescriptorFactory.fromResource(R.drawable.marker_destination);
    }

    public void onBackPressed() {
        switch(pricingState) {
            case ShowingPricing:
                setPricingState(PricingState.DestinationLocation);
                break;
            case Pricing:
                setPricingState(PricingState.DestinationLocation);
                break;
            case DestinationLocation:
                setPricingState(PricingState.StartLocation);
                break;
            default:
                if(currentServiceId != null  && !isUserProvider) {
                    super.onBackPressed();
                }
        }
        super.onBackPressed();
    }

    public void setPricingState(PricingState state) {
        pricingState = state;
        switch(state) {
            case StartLocation: {
                confirmStartButton.setVisibility(View.VISIBLE);
                destinationLocationView.setVisibility(View.GONE);
                addressTextView.setText("");
                if (startMarker != null) {
                    startMarker.remove();
                    removeRouteFromMapIfPresent();
                }
                startAddress = null;
                addrStart = null;
                lastLocationResult = null;
                lastAddressResult = null;
                proceedWithPricingButton.setEnabled(false);
            }
            break;
            case DestinationLocation: {
                confirmStartButton.setVisibility(View.GONE);
                confirmDestinationButton.setVisibility(View.VISIBLE);
                destinationLocationView.setVisibility(View.VISIBLE);
                proceedWithPricingButton.setVisibility(View.GONE);
                quotationInfoView.setVisibility(View.GONE);
                destinationAddressTextView.setText("");
                if (destinationMarker != null) {
                    destinationMarker.remove();
                    removeRouteFromMapIfPresent();
                }
                destinationAddress = null;
                addrDestination = null;
                lastLocationResult = null;
                lastAddressResult = null;
                proceedWithPricingButton.setEnabled(false);
            }
            break;
            case Pricing: {
                proceedWithPricingButton.setVisibility(View.VISIBLE);
                quotationInfoView.setVisibility(View.GONE);
                confirmDestinationButton.setVisibility(View.GONE);
                quotationCostTextView.setText(null);
                quotationDistanceTextView.setText(null);
                quotationTripDurationTextView.setText(null);
                proceedWithPricingButton.setText(R.string.text_get_quotation);
                proceedWithPricingButton.setEnabled(true);
            }
            break;
            case ShowingPricing: {
                quotationCostTextView.setText("$" + calculatedAmount);
                DistanceMatrix.Element distanceElement = distance.rows.get(0).elements.get(0);
                quotationDistanceTextView.setText(distanceElement.distance.text);
                quotationTripDurationTextView.setText(distanceElement.duration.text);
                proceedWithPricingButton.setText(R.string.text_order_service);
                proceedWithPricingButton.setEnabled(false);
                quotationInfoView.setVisibility(View.VISIBLE);
                if (mCurrentMarker != null) {
                    showProviderInfo();
                }
            }
            break;
            case NoState: {

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case DESTINATION_LOCATION:
                case START_LOCATION: {
                    String json = data.getStringExtra(Constants.KEY_ADDRESS_OBJECT);
                    LatLng location;
                    android.location.Address addressGoogle = null;
                    Address address = null;
                    if (Constants.USE_NATIVE_GEOCODER) {
                        addressGoogle = gson.fromJson(json, android.location.Address.class);
                        location = new LatLng(addressGoogle.getLatitude(), addressGoogle.getLongitude());
                    } else {
                        address = gson.fromJson(json, Address.class);
                        location = address.geometry.location.getLatLng();
                    }
                    boolean updatemap = false;
                    if (requestCode == START_LOCATION) {
                        if (Constants.USE_NATIVE_GEOCODER) {
                            addressTextView.setText(Utils.getFormattedAddress(addressGoogle));
                            setStartAddress(addressGoogle);
                        } else {
                            addressTextView.setText(address.formatted_address);
                            setStartAddress(address);
                        }

                        if (pricingState == PricingState.StartLocation) {
                            setPricingState(PricingState.DestinationLocation);
                            updatemap = true;
                        }
                    } else {
                        if (Constants.USE_NATIVE_GEOCODER) {
                            destinationAddressTextView.setText(Utils.getFormattedAddress(addressGoogle));
                            setDestinationAddress(addressGoogle);
                        } else {
                            destinationAddressTextView.setText(address.formatted_address);
                            setDestinationAddress(address);
                        }

                        if (pricingState == PricingState.DestinationLocation) {
                            setPricingState(PricingState.Pricing);
                            updatemap = true;
                        }
                    }
                    if (updatemap) {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, 17.0f);
                        mMap.animateCamera(cameraUpdate, 1, null);
                    }
                }
                break;
                case REGISTER_CARD:
                    createServiceRequest();
                    break;
            }
        }
    }

    public void getRoute(LatLng origin, LatLng destination) {
        String url=
                "http://maps.googleapis.com/maps/api/directions/json?origin="
                        + origin.latitude + "," + origin.longitude +"&destination="
                        + destination.latitude + "," + destination.longitude + "&sensor=false";

    }

    /* OnMarkerClickListener  & OnMapClickListener*/

    @Override
    public boolean onMarkerClick(Marker marker) {
        mCurrentMarker = marker;
        if (pricingState == PricingState.ShowingPricing && mMarkers.containsValue(marker)) { //one of the
            marker.showInfoWindow();
            showProviderInfo();
            proceedWithPricingButton.setEnabled(true);
            return true;
        }
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mCurrentMarker = null;
        if (pricingState == PricingState.ShowingPricing) {
            proceedWithPricingButton.setEnabled(false);
            showProviderInfo();
        }
    }


    /* Client side */

    public void startServiceStatus() {
        m_handler.post(serviceStatus);
    }

    public void stopServiceStatus() {
        m_handler.removeCallbacks(serviceStatus);
    }

    public void startGettingProviders()
    {
        m_handler.post(checkProviders);
    }

    public void stopGettingProviders()
    {
        m_handler.removeCallbacks(checkProviders);
    }

    private void clientStartServiceTracking() {
        m_handler.post(clientServiceTracking);
    }

    private void clientStopServiceTracking() {
        m_handler.removeCallbacks(clientServiceTracking);
    }

    public void createServiceRequest() {
        ProviderMapObject provider = getCurrentSelectedProvider();
        JsonObject object = new JsonObject();
        object.addProperty("quotation", new Long(myCurrentQuotation.getId()));
        object.addProperty("provider", new Long(provider.getProviderId()));
        Ion.with(MapsActivity.this).load(Constants.URL_PROVIDER_REQUEST_CREATE)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .setStringBody(object.toString())
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {

                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        if (e == null) {
                            HashMap<HashMap, Long> response = gson.fromJson(result.getResult(), new TypeToken<HashMap<String, Long>>() {
                            }.getType());
                            currentProviderRequestId = response.get("id");
                            final ServiceExistsResponse serviceResponse = gson.fromJson(result.getResult(), ServiceExistsResponse.class);
//                            myCurrentQuotation = serviceResponse.getQuotation();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    //Update UI
                                    loadingDialog = ProgressDialog.show(MapsActivity.this, "",
                                            "Creando petición de servicio...", true);
                                    loadingDialog.setCancelable(false);
                                    startServiceStatus();
                                }
                            });
                        }
                    }
                });
    }

    private void createService() {
        /* Create Service */
        JsonObject object = new JsonObject();
        ProviderMapObject provider = getCurrentSelectedProvider();
        if (provider == null) return;
        object.addProperty("quotationId", myCurrentQuotation.getId());
        object.addProperty("providerId", Long.parseLong(provider.getProviderId()));
        object.addProperty("id", currentProviderRequestId);
        Log.w("createService", object.toString());
        Ion.with(MapsActivity.this).load(Constants.URL_SERVICE_CREATE)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .setStringBody(object.toString())
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null) {
                            CreateServiceResponse response = gson.fromJson(result.getResult(), CreateServiceResponse.class);
                            if (response == null || response.id == null || response.id == 0) {
                                //ocurrió un error
                                startGettingService();
                            } else {
                                setCurrentServiceId(response.id);
                                clientStartServiceTracking();
                            }

                        }
                    }
                });
    }

    private void updateProvidersInMap(List<ProviderMapObject> providers) {

        HashSet<String> set = new HashSet<String>();
        if (providers == null) return;
        for (int i = 0; i < providers.size(); i++) {
            ProviderMapObject object = providers.get(i);
            String id = "" + object.getProviderId();
            Marker marker = mMarkers.get(id);
            if (marker == null) {
                MarkerOptions markerOptions = new MarkerOptions().position(object.getLatLng())
                        .title(object.getName())
                        .snippet(object.getFirstName() + " " + object.getLastName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck));
                Marker newMarker = mMap.addMarker(markerOptions);
                mMarkers.put(id, newMarker);
                set.add(id);
            } else {
                set.add(id);
                MarkerAnimation.animateMarkerToGB(marker, object.getLatLng(), interpolator);
            }
        }
        Set<String> existingKeys = mMarkers.keySet();
        ArrayList<String> keysToDelete = new ArrayList<>();
        for (String key : existingKeys) {
            if (!set.contains(key)) {
                keysToDelete.add(key);
            }
        }
        for (String key : keysToDelete) {
            Marker marker = mMarkers.get(key);
            mMarkers.remove(key);
            marker.remove();
        }
        mAllProviders.clear();
        mAllProviders.addAll(providers);
    }

    private void showProviderInfo() {
        ProviderMapObject provider = getCurrentSelectedProvider();
        if (provider != null) {
            quotationProvider.setText(provider.getName());
        } else {
            quotationProvider.setText("");
        }
    }

    private ProviderMapObject getCurrentSelectedProvider() {
        for (ProviderMapObject provider : mAllProviders) {
            String id = "" + provider.getProviderId();
            Marker marker = mMarkers.get(id);
            if (marker.equals(mCurrentMarker)) {
                return provider;
            }
        }
        return null;
    }

    void getQuotation() {

        LatLng start = getStartLatLng();
        LatLng destination = getDestinationLatLng();
        double sLat = start.latitude;
        double sLon = start.longitude;
        double dLat = destination.latitude;
        double dLon = destination.longitude;

        characteristics.sourceLatitude = sLat;
        characteristics.sourceLongitude = sLon;
        characteristics.destinationLatitude = dLat;
        characteristics.destinationLongitude = dLon;
        characteristics.distance = (float)distance.rows.get(0).elements.get(0).distance.value / 1000.0f;
        characteristics.description = "Prueba de cotización";

        final String bodyString = gson.toJson(characteristics);
        final Context context = this;
        Ion.with(this).load(Constants.URL_CREATE_QUOTATION)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .setStringBody(bodyString)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null) {
                            try {
                                myCurrentQuotation = gson.fromJson(result.getResult(), Quotation.class);
                                calculatedAmount = "" + myCurrentQuotation.getAmount(); /* To do: Crear un objeto de tipo Quotation*/
                                showQuotationInfo();
                            } catch(Exception ex) {

                            }
                        } else {

                        }
                    }
                });
    }

    public void getDistanceBetweenLocations() {
        String url = Constants.URL_DISTANCE_MATRIX;
        LatLng start = getStartLatLng();
        LatLng destination = getDestinationLatLng();
        double sLat = start.latitude;
        double sLon = start.longitude;
        double dLat = destination.latitude;
        double dLon = destination.longitude;

        url = url.replace("<sLat>", "" + sLat);
        url = url.replace("<sLon>", "" + + sLon);
        url = url.replace("<dLat>", "" + dLat);
        url = url.replace("<dLon>", "" + + dLon);
        Ion.with(this).load(url)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null) {
                            distance = gson.fromJson(result.getResult(), DistanceMatrix.class);
                            if (distance != null) {
                                getQuotation();
                            }
                        }
                    }
                });

    }


    /* Provider side */

    public void startServiceExists() {
        m_handler.post(serviceExists);
    }

    public void stopServiceExists()
    {
        m_handler.removeCallbacks(serviceExists);
    }

    private void providerStartServiceTracking() {
        m_handler.post(serviceTracking);
    }

    private void providerStopServiceTracking() {
        m_handler.removeCallbacks(serviceTracking);
    }

    private void showNewQuotationArrived() {

        final LinearLayout myQuotationInfoLayout = (LinearLayout)findViewById(R.id.serviceRequestInfoLayout);

        LatLng sLocation = new LatLng(myCurrentQuotation.getSourceLatitude(), myCurrentQuotation.getSourceLongitude());
        LatLng dLocation = new LatLng(myCurrentQuotation.getDestinationLatitude(), myCurrentQuotation.getDestinationLongitude());

        providerStartLocation = sLocation;
        providerDestinationLocation = dLocation;

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(sLocation, dLocation)
                .build();
        routing.execute();



        zoomOnRoute(sLocation, dLocation);

        MarkerOptions marker = new MarkerOptions();
        marker.position(sLocation);
        marker.icon(getBitmapDescriptor(true));
        startMarker = mMap.addMarker(marker);

        marker = new MarkerOptions();
        marker.position(dLocation);
        marker.icon(getBitmapDescriptor(false));
        destinationMarker = mMap.addMarker(marker);

        TextView serviceWidthTextView = (TextView)findViewById(R.id.serviceWeightTextView);
        TextView serviceVolumeDepthTextView = (TextView)findViewById(R.id.serviceVolumeDepthTextView);
        TextView serviceVolumeHeightTextView = (TextView)findViewById(R.id.serviceVolumeHeightTextView);
        TextView serviceVolumeWidthTextView = (TextView)findViewById(R.id.serviceVolumeWidthTextView);
        TextView serviceCostTextView = (TextView)findViewById(R.id.serviceCostTextView);
        TextView serviceDistanceTextView = (TextView)findViewById(R.id.serviceDistanceTextView);

        Button cancelButton = (Button)findViewById(R.id.cancelServiceButton);
        Button acceptlButton = (Button)findViewById(R.id.createServiceButton);

        myQuotationInfoLayout.setVisibility(View.VISIBLE);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = currentProviderRequestId;
                long status = 0;
                if (v.getId() == R.id.cancelServiceButton) {
                    status = 2;
                } else {
                    status = 1;
                    loadingDialog = ProgressDialog.show(MapsActivity.this, "",
                            "Cargando servicio...", true);
                    loadingDialog.setCancelable(false);
                }
                final long finalStatus = status;
                JsonObject object = new JsonObject();
                object.addProperty("id", new Long(id));
                object.addProperty("status", new Long(status));
                Ion.with(MapsActivity.this).load("PUT",Constants.URL_PROVIDER_REQUEST_UPDATE)
                        .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                        .addHeader("Content-Type", "application/json")
                        .setStringBody(object.toString())
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                if (e == null) {
                                    myQuotationInfoLayout.setVisibility(View.GONE);
                                    if (finalStatus == 2) {
                                        myCurrentQuotation = null;
                                        startMarker.remove();
                                        destinationMarker.remove();
                                        removeRouteFromMapIfPresent();
                                        startMarker = null;
                                        destinationMarker = null;
                                        startServiceExists();
                                    } else {
                                        fromMyLocation = myLocation;
                                        startGettingService();
                                    }
                                }
                            }
                        });

            }
        };

        cancelButton.setOnClickListener(listener);
        acceptlButton.setOnClickListener(listener);

        serviceWidthTextView.setText("" + myCurrentQuotation.getWeight());
        serviceVolumeDepthTextView.setText("" + myCurrentQuotation.getVolumeDepth());
        serviceVolumeHeightTextView.setText("" + myCurrentQuotation.getVolumeHeight());
        serviceVolumeWidthTextView.setText("" + myCurrentQuotation.getVolumeWidth());
        serviceCostTextView.setText("" + myCurrentQuotation.getAmount());
        serviceDistanceTextView.setText("" + myCurrentQuotation.getDistance());
    }

    private void startGettingService() {
        m_handler.post(gettingServiceRunnable);
    }

    private void stopGettingService() {
        m_handler.removeCallbacks(gettingServiceRunnable);
    }
    /* Map showing operations */


    public void zoomOnRoute(LatLng origin, LatLng destination) {
        if (origin == null || destination == null) return;
        final LinearLayout myQuotationInfoLayout = (LinearLayout)findViewById(R.id.serviceRequestInfoLayout);
        double neLat = destination.latitude > origin.latitude ? destination.latitude : origin.latitude;
        double neLon = destination.longitude > origin.longitude ? destination.longitude : origin.longitude;
        double swLat = destination.latitude < origin.latitude ? destination.latitude : origin.latitude;
        double swLon = destination.longitude < origin.longitude ? destination.longitude : origin.longitude;

        LatLng ne = new LatLng(neLat, neLon);
        LatLng sw = new LatLng(swLat, swLon);

        LatLng centerCoord = midPoint(origin.latitude, origin.longitude, destination.latitude, destination.longitude);

        LatLngBounds bounds = new LatLngBounds(sw, ne);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,
                (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics()));
//        cameraUpdate = CameraUpdateFactory.newLatLngZoom(centerCoord, 15);
        mMap.animateCamera(cameraUpdate, 1,null);
    }

    public LatLng midPoint(double lat1,double lon1,double lat2,double lon2){

//        double dLon = Math.toRadians(lon2 - lon1);

        //convert to radians
//        lat1 = Math.toRadians(lat1);
//        lat2 = Math.toRadians(lat2);
//        lon1 = Math.toRadians(lon1);
//
//        double Bx = Math.cos(lat2) * Math.cos(dLon);
//        double By = Math.cos(lat2) * Math.sin(dLon);
//        double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
//        double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

        double lat3 = (lat2+lat1)/2;
        double lon3 = (lon2 + lon1) / 2;
        //print out in degrees
        return new LatLng(lat3, lon3);
    }

    private void removeRouteFromMapIfPresent() {
        if(polylines.size()>0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
            polylines = new ArrayList<>();
        }
    }

    /* Runnables */

    private class ProviderServiceStatus implements Runnable {

        @Override
        public void run() {
            Ion.with(MapsActivity.this).load(Constants.URL_PROVIDER_REQUEST_STATUS + "/" + currentProviderRequestId)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {

                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            if (e == null) {
                                final ServiceExistsResponse serviceResponse = gson.fromJson(result.getResult(), ServiceExistsResponse.class);
                                if (serviceResponse.getStatus() == 0) {

                                } else if (serviceResponse.getStatus() == 1) { //aceptado
                                    loadingDialog.dismiss();
                                    new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Deliver App")
                                            .setContentText("El proveedor aceptó el servicio")
                                            .show();
                                    stopServiceStatus();
                                    createService();
                                    currentServiceStatus = ServiceStatus.OnItsWayToOrigin;

                                    stopGettingProviders();
                                    quotationInfoView.setVisibility(View.GONE);
                                    proceedWithPricingButton.setVisibility(View.GONE);
                                    pickPlacesLayout.setVisibility(View.GONE);
                                    removeAllProvidersFromMap();

                                } else if (serviceResponse.getStatus() == 2) { //cancelado
                                    loadingDialog.dismiss();
                                    new SweetAlertDialog(MapsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Deliver App")
                                            .setContentText("El proveedor declinó el servicio, te recomendamos buscar otro proveedor.")
                                            .show();
                                    stopServiceStatus();
                                }

                            }
                        }
                    });
            m_handler.postDelayed(serviceStatus, m_interval);

        }
    }

    private class GetProviders implements Runnable {

        @Override
        public void run() {
            JsonObject object = new JsonObject();
            object.addProperty("latitude", myLocation.getLatitude());
            object.addProperty("longitude", myLocation.getLongitude());
            Log.w("getProviders", "Request: " + object.toString());
            Ion.with(MapsActivity.this).load(Constants.URL_FIND_ALL_NEARBY)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
                    .setStringBody(object.toString())
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {

                            if (e == null) {
                                String resultStr = result.getResult();
                                Type type = new TypeToken<ArrayList<ProviderMapObject>>(){}.getType();
                                final List<ProviderMapObject> providers = gson.fromJson(resultStr, type);
                                Log.w("getProviders", "Response: " + result.getResult());
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        //Update UI
                                        updateProvidersInMap(providers);
                                    }
                                });
                            }
                        }
                    });
            m_handler.postDelayed(checkProviders, m_interval);
        }
    }

    private class ServiceExists implements Runnable {

        @Override
        public void run() {
            JsonObject object = new JsonObject();
            Log.w("ServiceExists", "Request: " + object.toString());
            Ion.with(MapsActivity.this).load(Constants.URL_PROVIDER_REQUEST_SERVICE_EXISTS)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
//                    .setStringBody(object.toString())
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {

                        @Override
                        public void onCompleted(Exception e, Response<String> result) {

                            if (e == null) {
                                Type type = new TypeToken<List<ServiceExistsResponse>>(){}.getType();
                                Log.w(this.getClass().toString(), ""+ result.getResult());
                                final List<ServiceExistsResponse> serviceResponses = gson.fromJson(result.getResult(), type);
                                if (serviceResponses.size() > 0) {
                                    ServiceExistsResponse serviceResponse = serviceResponses.get(0);
                                    currentProviderRequestId = serviceResponse.getId();

                                    if (myCurrentQuotation == null) {
                                        Log.w("ServiceExists", "Id: " + currentProviderRequestId);
                                        myCurrentQuotation = serviceResponse.getQuotation();
                                        runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                //Update UI
                                                if (myCurrentQuotation != null) {
                                                    stopServiceExists();
                                                    showNewQuotationArrived();
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
            m_handler.postDelayed(serviceExists, m_interval);
        }
    }

    private class GettingService implements Runnable {

        @Override
        public void run() {
            String url = Constants.URL_SERVICE_FIND_BY_QUOTATION + myCurrentQuotation.getId();
            Ion.with(MapsActivity.this).load(url)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {

                        @Override
                        public void onCompleted(Exception e, Response<String> result) {

                            if (e == null) {
                                loadingDialog.dismiss();
                                final GettingServiceResponse serviceResponse = gson.fromJson(result.getResult(), GettingServiceResponse.class);
                                if (serviceResponse.getError() == null && serviceResponse.getId() != 0) {

                                    if (isUserProvider) {
                                        quotationInfoView.setVisibility(View.INVISIBLE);
                                        setCurrentServiceId(serviceResponse.getId());
                                        showRouteToClient();
                                        stopGettingService();
                                        setCurrentServiceStatus(ServiceStatus.OnItsWayToOrigin);
                                        providerStartServiceTracking();
                                    } else {
                                        setCurrentServiceId(serviceResponse.getId());
                                        clientStartServiceTracking();
                                    }
                                }
                            }
                        }
                    });
            m_handler.postDelayed(gettingServiceRunnable, m_interval);
        }
    }

    private class ProviderServiceTracking implements Runnable {

        @Override
        public void run() {
            JsonObject object = new JsonObject();
            object.addProperty("latitude", myLocation.getLatitude());
            object.addProperty("longitude", myLocation.getLongitude());
            object.addProperty("id", currentServiceId);
            object.addProperty("status", currentServiceStatus.getCode());
            Log.w(this.getClass().toString(), "Request: " + object.toString());
            Ion.with(MapsActivity.this).load("PUT",Constants.URL_SERVICE_UPDATE)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
                    .setStringBody(object.toString())
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {

                            if (e == null) {
                                if (result.getHeaders().code() == 200) {
                                    Log.w("ServiceTracking", "Tracking succeded");
                                } else {
                                    Log.w("ServiceTracking", "Response: " + result.getResult());
                                }
                            }
                        }
                    });
            if (currentServiceStatus == ServiceStatus.DeliveryFinished) {

                resetProviderState();
                providerStopServiceTracking();
            } else {
                m_handler.postDelayed(serviceTracking, m_interval);
            }
        }
    }

    private void resetProviderState() {
        setCurrentServiceStatus(ServiceStatus.NoService);

        startAddress = null;
        destinationAddress = null;

        addrStart = null;
        addrDestination = null;

        pricingState = PricingState.StartLocation;
        if (startMarker != null) {
            startMarker.remove();
            startMarker = null;
        }
        if (destinationMarker != null) {
            destinationMarker.remove();
            destinationMarker = null;
        }

        myCurrentQuotation = null;
        currentProviderRequestId = 0L; //For handshake between client and provider
        currentServiceId = 0L; //For when the service has been confirmed

        removeRouteFromMapIfPresent();

        fromMyLocation = null;
        providerStartLocation = null;
        providerDestinationLocation = null;

        if (currentRoute != null) {
            currentRoute = null;
        }
        if (trackingProviderMarker != null) {
            trackingProviderMarker.remove();
            trackingProviderMarker = null;
        }

        beginTripButton.setVisibility(View.INVISIBLE);

        startServiceExists();
    }

    private void resetClientState() {
        removeRouteFromMapIfPresent();
        removeAllProvidersFromMap();
        setCurrentServiceStatus(ServiceStatus.NoService);
        pricingState = PricingState.StartLocation;

        if(destinationMarker != null) {
            destinationMarker.remove();
            destinationMarker = null;
        }
        if (startMarker != null) {
            startMarker.remove();
            startMarker = null;
        }

        myCurrentQuotation = null;
        currentProviderRequestId = 0L; //For handshake between client and provider
        currentServiceId = 0L; //For when the service has been confirmed

        startAddress = null;
        destinationAddress = null;

        addrStart = null;
        addrDestination = null;

        distance = null;

        Intent scanIntent = new Intent(this, ServicioLogistico.class);
        scanIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(scanIntent);
//        startGettingProviders();
    }

    private class ClientServiceTracking implements Runnable {

        @Override
        public void run() {
            String url = Constants.URL_SERVICE_GET + "/" + currentServiceId;
            Log.w(this.getClass().toString(), url);
            Ion.with(MapsActivity.this).load(url)
                    .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                    .addHeader("Content-Type", "application/json")
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            if (e == null) {
                                if (result.getHeaders().code() == 200) {
                                    if (e == null) {
                                        String resultStr = result.getResult();
                                        final ClientTrackingResponse response = gson.fromJson(resultStr, ClientTrackingResponse.class);
                                        if (response.getError() == null) {
                                            setCurrentServiceStatus(ServiceStatus.serviceFromCode(response.getStatus()));
                                            runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    //Update UI
                                                    if (response.getLongitude() != 0 && response.getLatitude() != 0) {
                                                        response.getProvider().setLatitude(response.getLatitude());
                                                        response.getProvider().setLongitude(response.getLongitude());
                                                    }
                                                    updateTrackingInMap(response.getProvider(), false);
                                                }
                                            });

                                        } else {

                                        }
//                                        final List<ProviderMapObject> providers = gson.fromJson(resultStr, type);
                                        Log.w("ClientServiceTracking", "Response: " + result.getResult());

                                    }
                                }
                            }
                        }
                    });
            m_handler.postDelayed(clientServiceTracking, m_interval);
        }
    }

    private void updateTrackingInMap(ProviderMapObject provider, boolean delete) {
        HashSet<String> set = new HashSet<String>();
        if (provider == null) return;

        ProviderMapObject object = provider;
        String id = "" + object.getProviderId();
        if (!delete) {

            switch(currentServiceStatus) {
                case OnItsWayToOrigin:
                case InTransitToDestination:
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(object.getLatLng(), 16);
                    mMap.animateCamera(cameraUpdate);
                    break;
                case DeliveryFinished:
                    clientStopServiceTracking(); //finish serviceTracking on client
                    showRateScreen();
                    break;
            }
            if (trackingProviderMarker == null) {
                MarkerOptions markerOptions = new MarkerOptions().position(object.getLatLng())
                        .title(object.getName())
                        .snippet(object.getFirstName() + " " + object.getLastName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck));
                trackingProviderMarker = mMap.addMarker(markerOptions);
            } else {
                MarkerAnimation.animateMarkerToGB(trackingProviderMarker, object.getLatLng(), interpolator);
            }
        } else {
            if (trackingProviderMarker != null) {
                trackingProviderMarker.remove();
                trackingProviderMarker = null;
            }
        }
    }

    private void showRateScreen() {
        ratingContainer.setVisibility(View.VISIBLE);
    }

    /* Async Tasks */

    private class GetAddressFromLocationAsyncTask extends AsyncTask<LatLng,Void , android.location.Address> {

        @Override
        protected android.location.Address doInBackground(LatLng... params) {
            LatLng location = params[0];
            List<android.location.Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(location.latitude, location.longitude,1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                android.location.Address address = addresses.get(0);
                return address;
            }
            return null;
        }

        @Override
        protected void onPostExecute(android.location.Address address) {
            super.onPostExecute(address);
            if (address == null)
                return;
            lastAddressResult = address;
            if (pricingState == PricingState.StartLocation) {
                addressTextView.setText(Utils.getFormattedAddress(address));
            } else {
                destinationAddressTextView.setText(Utils.getFormattedAddress(address));
            }
        }
    }


    /* RoutingListener */

    @Override
    public void onRoutingFailure(RouteException e) {
        Toast.makeText(this, "Route Failed: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT);
    }

    @Override
    public void onRoutingStart() {
        Toast.makeText(this, "Route Start", Toast.LENGTH_SHORT);
    }

    @Override
    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
        removeRouteFromMapIfPresent();
        //add route(s) to the map.
//        for (int i = 0; i <route.size(); i++) {

        //In case of more than 5 alternative routes



        if (fromMyLocation == null) {
            currentRoute = route.get(0);
            zoomOnRoute(getStartLatLng(), getDestinationLatLng());
        } else {
            if (addrStart != null && addrDestination != null) {
                zoomOnRoute(new LatLng(fromMyLocation.getLatitude(), fromMyLocation.getLongitude()), getStartLatLng());
            }
        }

        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.width(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
        if (fromMyLocation != null) {
            showRouteOnMap(route.get(0), 0xff31aa39, true);
        } else {
            showRouteOnMap(route.get(0), 0xffd60000, true);
        }
        if (myCurrentQuotation != null && !isUserProvider) {
            getDistanceBetweenLocations();
        }
//            Toast.makeText(getApplicationContext(),"Route "+ (i+1) +": distance - "+ route.get(i).getDistanceValue()+": duration - "+ route.get(i).getDurationValue(),Toast.LENGTH_SHORT).show();
//        }
    }

    private float distanceBetween(LatLng location1, LatLng location2) {
        float[] results = new float[1];
        Location.distanceBetween(location1.latitude, location1.longitude, location2.latitude, location2.longitude, results);
        return results[0];
    }

    private float distanceBetween(Location location1, Location location2) {
        float[] results = new float[1];
        Location.distanceBetween(location1.getLatitude(), location1.getLongitude(), location2.getLatitude(), location2.getLongitude(), results);
        return results[0];
    }

    @Override
    public void onRoutingCancelled() {
        Toast.makeText(this, "Route Cancelled", Toast.LENGTH_SHORT);
    }

    public void showRouteToClient() {

        removeRouteFromMapIfPresent();
        LatLng myLocation = new LatLng(fromMyLocation.getLatitude(), fromMyLocation.getLongitude());
        LatLng clientLocation = providerStartLocation;

        List<LatLng> waypoints = new ArrayList<>();
        waypoints.add(myLocation);
        waypoints.add(clientLocation);
        getRoute(waypoints);
    }

    public LatLng getStartLatLng() {
        if (isUserProvider) {
            return providerStartLocation;
        } else if (addrStart == null && startAddress == null) {
            return null;
        }
        return Constants.USE_NATIVE_GEOCODER ? new LatLng(addrStart.getLatitude(), addrStart.getLongitude()) : startAddress.geometry.location.getLatLng();
    }

    public LatLng getDestinationLatLng() {
        if (isUserProvider) {
            return providerStartLocation;
        } else if (addrDestination == null && destinationAddress == null)
            return null;
        return Constants.USE_NATIVE_GEOCODER ? new LatLng(addrDestination.getLatitude(), addrDestination.getLongitude()) : destinationAddress.geometry.location.getLatLng();
    }

    public void removeAllProvidersFromMap() {
        Set<String> existingKeys = mMarkers.keySet();
        for (String key : existingKeys) {
                Marker marker = mMarkers.get(key);
                marker.remove();
        }
        mMarkers.clear();
        mAllProviders.clear();
    }

    public void showRouteOnMap(Route route, int color, boolean clear) {
        if (clear) {
            removeRouteFromMapIfPresent();
        }
        List<LatLng> points = route.getPoints();
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(0xffd60000);
        polyOptions.width(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
        polyOptions.addAll(points);
        Polyline secondPolyline = mMap.addPolyline(polyOptions);
        polylines.add(secondPolyline);
    }

    public void getRoute(List<LatLng> waypoints) {
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(waypoints)
                .build();
        routing.execute();
    }
}
