package com.deliver.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.deliver.R;
import com.deliver.customobjects.DeliverCard;
import com.deliver.utils.Constants;
import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Future;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

public class RegistroFormaPagoTDTC extends BaseActivity implements View.OnClickListener {

    private static final int CARD_REQUEST_CODE = 30;
    private Button btnEscanear;
    private Button btnEnviar;
    private CreditCard scanResult;
    private EditText expirationEdit;
    private EditText cardNumberEdit;
    private EditText cvvEdit;
    private Button btnSkip;
    private boolean resultIsNeeded;
    private StringBuilder numberBuilder = new StringBuilder();
    private TextWatcher cardNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String number = s.toString().replace(" ", "");
            StringBuilder builder = new StringBuilder(number);
            cardNumberEdit.removeTextChangedListener(this);
            if (count != 0) {
                numberBuilder.append(number.substring(number.length() - 1, number.length()));
            } else if (!numberBuilder.toString().isEmpty()){
                numberBuilder.deleteCharAt(numberBuilder.length() - 1);
            }
            for (int i = 0; i < 12 && i<builder.length(); i++) {
                builder.replace(i, i+1, "•");
            }

            try{
                builder.insert(4, " ");
                builder.insert(9, " ");
                builder.insert(14, " ");
            } catch(IndexOutOfBoundsException e) {

            }
            if (count == 0 && number.length() > 0 && number.length()%4 == 0) {
                builder.replace(builder.length() - 1, builder.length(), "");
            }
            cardNumberEdit.setText(builder.toString());
            cardNumberEdit.addTextChangedListener(this);
            cardNumberEdit.setSelection(cardNumberEdit.getText().length());
            if (numberBuilder.length() == 16) {
                View nextField = cardNumberEdit.focusSearch(View.FOCUS_FORWARD);
                nextField.requestFocus();
                cardNumber = numberBuilder.toString();
            } else {
                cardNumber = null;
            }
            }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher expiryDateTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count != 0) {
                String number = s.toString().replace(" ", "");
                if (number.length() == 1) {
                    int month = Integer.parseInt(number);
                    if (month > 1) {
                        number = "0" + number + "/";
                    }
                } else if (number.length() == 2) {
                    int month = Integer.parseInt(number);
                    if (month > 12) {
                        number = "1";
                    } else {
                        number = number + "/";
                    }
                }


                expirationEdit.removeTextChangedListener(this);
                expirationEdit.setText(number);
                expirationEdit.addTextChangedListener(this);
                expirationEdit.setSelection(expirationEdit.getText().length());
                if (number.length() == 5) {
                    View nextField = expirationEdit.focusSearch(View.FOCUS_RIGHT);
                    nextField.requestFocus();
                    expiryDate = number;
                } else {
                    expiryDate = null;
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private String cardNumber;
    private String expiryDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_forma_pago_tdtc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cardNumberEdit = (EditText)findViewById(R.id.editTextNumeroTarjeta);
        expirationEdit = (EditText)findViewById(R.id.editTextMMAA);
        cvvEdit = (EditText)findViewById(R.id.editTextCodigoSeguridad);

        cardNumberEdit.addTextChangedListener(cardNumberTextWatcher);
        expirationEdit.addTextChangedListener(expiryDateTextWatcher);

        btnEscanear = (Button)findViewById(R.id.btnEscanear);
        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        btnSkip = (Button)findViewById(R.id.btnSkip);

        btnEscanear.setOnClickListener(this);
        btnEnviar.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        if (getCallingActivity() != null) {
            resultIsNeeded = true;
            btnSkip.setText(R.string.dialog_cancel);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnEscanear:
            {
                Intent scanIntent = new Intent(this, CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, CARD_REQUEST_CODE);
            }
            break;
            case R.id.btnEnviar:
            {
                if (scanResult == null && !validateFields())  {
                    showErrorMessage("Datos incompletos.\nPor favor llena todos los campos", null);
                    return;
                }
                final Activity activity = this;

                Conekta.setPublicKey(Constants.CONEKTA_API_KEY); //Set public key
                Conekta.setApiVersion("1.0.0"); //Set api version (optional)
                Conekta.collectDevice(activity); //Collect device

                final String cardNumber = scanResult != null ? scanResult.cardNumber : this.cardNumber;
                String cvv = scanResult != null ? scanResult.cvv : cvvEdit.getText().toString();
                String expiryMonth = null;
                String expiryYear = null;
                if (scanResult != null) {
                    expiryMonth = "" + scanResult.expiryMonth;
                    expiryMonth = "" + scanResult.expiryYear;
                } else {
                    String []expiryDateArr = expiryDate.split("/");
                    expiryMonth = expiryDateArr[0];
                    expiryYear = expiryDateArr[1];

                }

                Card card = new Card("Josue Camara", cardNumber,cvv, expiryMonth, expiryYear);
                Token token = new Token(activity);
                showLoading("Registrando Tarjeta");
                token.onCreateTokenListener( new Token.CreateToken(){
                    @Override
                    public void onCreateTokenReady(JSONObject data) {

                        try {
                            //TODO: Create charge
                            String token = data.getString("id");
                            registerCard(token, cardNumber);
                        } catch (Exception err) {
                            //TODO: Handle error
                            Log.d("Conekta Token","Error: " + err.toString());
                            dismissLoading();
                            new SweetAlertDialog(RegistroFormaPagoTDTC.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Deliver App")
                                    .setContentText("Ocurrio un error al registrar la tarjeta, por favor verifica tus datos.\n\n" + err.getLocalizedMessage() )
                                    .setConfirmText("Aceptar")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();

                        }
                    }
                });
                token.create(card);
            }
            break;
            case R.id.btnSkip:
            {
                if (resultIsNeeded){
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                } else {
                    Intent scanIntent = new Intent(this, ServicioLogistico.class);
                    finish();
                    startActivity(scanIntent);
                }
            }
            break;
        }
    }

    private boolean validateFields() {

        if (cardNumber == null) return false;
        if (expiryDate == null) return false;
        if (cvvEdit.getText().length() != 3) return false;
        return true;
    }

    private void registerCard(String token, String cardNumber) {

        JsonObject object = new JsonObject();

        String last4Digits = null;

        last4Digits = (String)cardNumber.subSequence(cardNumber.length() - 4, cardNumber.length());
        object.addProperty("token", token);
        object.addProperty("number", last4Digits);

        String bodyString = object.toString();
        final String cardNumberLast4Digits = last4Digits;
        final Context context = this;
        Future<String> future= Ion.with(this).load(Constants.URL_CREATE_CARD)
                .setHeader("Authorization", Utils.getLoggedAuthorizationString())
                .setHeader("Content-Type", "application/json")
                .setStringBody(bodyString)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        dismissLoading();
                        if (e == null) {
                            if (!resultIsNeeded) {
                                if (checkForInvalidSessionInResponse(result, true)) {
                                    return;
                                }
                            }
                            try {
                                JSONObject object = new JSONObject(result);
                                DeliverCard newCard = new DeliverCard();
                                newCard.cardNumber = cardNumberLast4Digits;
                                newCard.token = object.getString("token");
                                Utils.saveCard(RegistroFormaPagoTDTC.this, newCard);
                                Toast.makeText(RegistroFormaPagoTDTC.this, "Registro exitoso", Toast.LENGTH_SHORT);
                                clearData();
                                new SweetAlertDialog(RegistroFormaPagoTDTC.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Deliver App")
                                        .setContentText("Se ha registrado la tarjeta **** **** **** " + cardNumberLast4Digits + " de manera exitosa")
                                        .setConfirmText("Aceptar")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                if (resultIsNeeded) {
                                                    setResult(Activity.RESULT_OK);
                                                    finish();
                                                } else {
                                                    Intent scanIntent = new Intent(RegistroFormaPagoTDTC.this, ServicioLogistico.class);
                                                    finish();
                                                    startActivity(scanIntent);
                                                }
                                            }
                                        })
                                        .show();
                            } catch (JSONException e1) {
                                new SweetAlertDialog(RegistroFormaPagoTDTC.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Deliver App")
                                        .setContentText("Ocurrio un error al registrar la tarjeta, por favor verifica tus datos.\n\n" + e1.getLocalizedMessage() )
                                        .setConfirmText("Aceptar")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();

                                            }
                                        })
                                        .show();
                            }
                        } else {
                            new SweetAlertDialog(RegistroFormaPagoTDTC.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Deliver App")
                                    .setContentText("Ocurrio un error al registrar la tarjeta, por favor verifica tus datos.\n\n" + e.getLocalizedMessage() )
                                    .setConfirmText("Aceptar")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    })
                                    .show();
                        }

                    }

                });
    }

    private void clearData() {
        expirationEdit.setText("");
        cvvEdit.setText("");
        cardNumberEdit.setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CARD_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                cardNumberEdit.setText(scanResult.getRedactedCardNumber());
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    expirationEdit.setText(scanResult.expiryMonth + "/" + scanResult.expiryYear);
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    cvvEdit.setText(scanResult.cvv);
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            Log.d("SCAN", resultDisplayStr);
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    @Override
    public void onBackPressed() {
        if (!resultIsNeeded) {
            super.onBackPressed();
        } else {
            setResult(RESULT_CANCELED);
            super.onBackPressed();
        }
    }
}
