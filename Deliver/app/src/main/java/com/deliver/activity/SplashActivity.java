package com.deliver.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;

import com.deliver.DeliverApplication;
import com.deliver.R;
import com.deliver.customobjects.responses.WhoIAmResponse;
import com.deliver.services.LocationService;
import com.deliver.utils.Constants;
import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Future;

public class SplashActivity extends BaseActivity {


    private static final long SPLASH_TIME_OUT = 3500;
    private static final int WRITE_PERMISSION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        hideTopBar();
        if (Constants.USE_UBERTESTERS) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION);
            } else {
                initUbertesters();
                nextScreen();
            }
        } else
        {
            nextScreen();
        }
    }

    private void moveForward() {
        Intent intent = new Intent(this, ServicioLogistico.class);
        startActivity( intent);
        finish();
    }

    public void onBackPressed() {

    }
    private void RefreshToken() {
        showLoading("Iniciando sesión...");
        OauthInfo info = OauthInfo.getInstance();
        String bodyString = "grant_type=refresh_token&refresh_token="+ info.getRefreshToken()
                + "&client_secret=" + Constants.CLIENT_SECRET
                + "&client_id="+Constants.CLIENT_ID;
        final Context context = this;
        Future<String> future= Ion.with(this).load(Constants.URL_TOKEN)
                .setHeader("Authorization", Utils.getAuthorizationString())
                .setHeader("Content-Type", "application/x-www-form-urlencoded")
                .setStringBody(bodyString)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            try {
                                JSONObject object = new JSONObject(result);
                                if (object.has("error")) {
                                    Utils.saveOauthInfo(SplashActivity.this, null);
                                    nextScreen();
                                    return;
                                }
                                OauthInfo info = new OauthInfo();
                                info.setAccessToken(object.getString("access_token"));
                                info.setRefreshToken(object.getString("refresh_token"));
                                info.setExpiresIn(object.getInt("expires_in"));
                                DeliverApplication.getInstance().setToken(object.getString("access_token"));
                                Utils.saveOauthInfo(SplashActivity.this, info);
                                moveForward();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                        } else {

                        }
                    }
                });

    }

    public void nextScreen() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent intent = new Intent();
                OauthInfo info = Utils.readOauthInfo(SplashActivity.this);
                if (info  == null) {
                    intent.setClass(SplashActivity.this, InicioSesion.class);
                    overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in);
                    startActivity(intent);
                    finish();
                } else {
                    if (info.isSessionValid()) {
                        DeliverApplication.getInstance().setToken(info.getAccessToken());
                        retrieveInfo();
                    } else {
                        RefreshToken();
                    }
                }


                // close this activity

            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initUbertesters();
                    nextScreen();
                }
                return;
            }
        }
    }

    private void initUbertesters() {
//        Ubertesters.initialize(DeliverApplication.getInstance(), LockingMode.DisableUbertesters, ActivationMode.Widget);
    }

    private void retrieveInfo() {
        DeliverApplication.getInstance().retrieveInformation(new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                dismissLoading();
                if (e == null) {
                    String json = result.getResult();
                    if (checkForInvalidSessionInResponse(json, false)) {
                        Utils.saveOauthInfo(SplashActivity.this, null);
                        nextScreen();
                        return;
                    }
                    Gson gson = new Gson();
                    WhoIAmResponse response = gson.fromJson(result.getResult(), WhoIAmResponse.class);
                    DeliverApplication.getInstance().processWhiIamResponse(response);
                    Log.d("Who I am Response", "" + response);
                    if (!DeliverApplication.getInstance().isUserProvider()) {
                        moveForward();
                    } else {
                        if (!isServiceRunning(LocationService.class)) {
                            DeliverApplication.getInstance().startGettingLocations();
                            startLocationService();
                        }
                        moveToMapAsProvider();
                    }
                } else {
                }
            }
        });
    }
}
