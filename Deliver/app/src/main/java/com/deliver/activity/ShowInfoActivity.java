package com.deliver.activity;

import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.deliver.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ShowInfoActivity extends BaseActivity implements View.OnClickListener {

    private TextView infoTextView;
    private LinearLayout acceptDeclineLayout;
    private Button btnAccept;
    private Button btnDecline;
    private ScrollView scrollView;

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_accept:
                setResult(RESULT_OK);
                break;
            case R.id.btn_decline:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    public enum InfoType {
        TermsAndConditions,
        Privacy,
        Restrictions;

        public int getTitleId() {
            switch(this) {
                case Privacy:
                    return R.string.privacy_text;
                case Restrictions:
                    return R.string.restrictions_text;
                case TermsAndConditions:
                    return R.string.terms_and_conditions;
            }
            return 0;
        }
    }

    InfoType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);
        showBackButton();
        type = (InfoType)getIntent().getExtras().getSerializable("type");
        setTitle(type.getTitleId());

        infoTextView = (TextView)findViewById(R.id.infoTextView);
        acceptDeclineLayout = (LinearLayout)findViewById(R.id.accept_decline);

        btnAccept = (Button)findViewById(R.id.btn_accept);
        btnDecline = (Button)findViewById(R.id.btn_decline);
        btnAccept.setOnClickListener(this);
        btnAccept.setEnabled(false);
        btnDecline.setOnClickListener(this);

        scrollView = (ScrollView)findViewById(R.id.scrollView);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (diff == 0) {
                    // do stuff
                    btnAccept.setEnabled(true);
                }
            }
        });
        fillInfo();
    }

    private void fillInfo() {
        switch(type) {
            case Privacy:
                infoTextView.setText(R.string.privacy);
                break;
            case Restrictions:
                infoTextView.setText(R.string.restrictions);
                break;
            case TermsAndConditions:
                infoTextView.setText(Html.fromHtml(getString(R.string.terms_and_conditions_text)));
                acceptDeclineLayout.setVisibility(View.VISIBLE);
                break;
        }
    }


}
