package com.deliver.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.deliver.DeliverApplication;
import com.deliver.R;
import com.deliver.customobjects.DeliverCard;
import com.deliver.utils.Constants;
import com.google.android.gms.common.api.GoogleApiClient;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;



import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;

import com.google.gson.JsonObject;

import com.koushikdutta.ion.Ion;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.conekta.conektasdk.Card;


public class CambiarDatosUsuario extends BaseActivity implements View.OnClickListener {


    private EditText editNombre;
    private EditText editApellido;
    private EditText editNumeroTelefonoLada;
    private EditText editNuwNumTelefono;
    private EditText editPassword;
    private EditText editNewPassword;
    private EditText editConfirmPassword;
    private EditText editEmail;

    private Button btnModificadoEnviar;
    private TextView numTarjeta;


    private CheckBox checkShowPassword;

    private EditText editUserName;

    Button btn_FormasPago, btn_FB, btn_AvisoPrivacidad;

    private GoogleApiClient client;
    private Button btnModificarTarjeta;
    private Button btnCerrarSesion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_datos_usuario);

        btnModificadoEnviar = (Button)findViewById(R.id.btn_RegistroModificadoEnviar);
        btnModificadoEnviar.setOnClickListener(this);

        checkShowPassword = (CheckBox)findViewById(R.id.checkBox);
        //checkShowPassword.setOnCheckedChangeListener(this);

        editEmail = (EditText)findViewById(R.id.editTextemail);
        editNombre = (EditText)findViewById(R.id.editTextNombre);
        editApellido = (EditText)findViewById(R.id.editTextApellido);
        editNumeroTelefonoLada = (EditText)findViewById(R.id.editnuevoNumeroTelefonoLada);
        editNuwNumTelefono = (EditText)findViewById(R.id.editNuevoNumeroTelefono);
        editPassword = (EditText)findViewById(R.id.editPassword);
        editNewPassword = (EditText)findViewById(R.id.editNuevoPassword);
        editConfirmPassword =(EditText)findViewById(R.id.editConfirmPassword);

        numTarjeta = (TextView)findViewById(R.id.xxx);
        btnModificarTarjeta = (Button)findViewById(R.id.btn_Actualizar_Tarjetas);
        btnModificarTarjeta.setOnClickListener(this);

        btnCerrarSesion = (Button)findViewById(R.id.btn_logoff);
        btnCerrarSesion.setOnClickListener(this);
    }

    public void onStart() {
        super.onStart();
        fillInfo();
    }

    public void fillInfo() {
        List<DeliverCard> cards = Utils.readCards(this);
        DeliverCard card = cards != null && !cards.isEmpty() ? cards.get(0) : null;
        if (card != null) {
            numTarjeta.setText("XXXX-XXXX-XXXX-"+card.cardNumber);
        } else {
            numTarjeta.setText("Sin tarjeta");
        }
    }

    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.btn_FormasPago:
                intent.setClass(this, RegistroFormadePago.class);
                break;
            case R.id.btn_Actualizar_Tarjetas:
            {
                intent.setClass(this, RegistroFormaPagoTDTC.class);
                startActivityForResult(intent, Constants.CHANGE_CARD_REQUEST_CODE);
            }

                break;
            case R.id.btn_AvisoPrivacidad:

                break;
            case R.id.btn_RegistroModificadoEnviar: {
                if (editEmail.getText().toString().isEmpty() || editNombre.getText().toString().isEmpty() || editApellido.getText().toString().isEmpty()
//                        || editPassword.getText().toString().isEmpty() || editPassword.getText().toString().isEmpty()
                        || editNuwNumTelefono.getText().toString().isEmpty() || editNumeroTelefonoLada.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Faltan datos", Toast.LENGTH_SHORT).show();
                } else {

                    JsonObject object = new JsonObject();

                    String telephone = editNumeroTelefonoLada.getText().toString() + editNuwNumTelefono.getText().toString();

                    object.addProperty("email", editEmail.getText().toString());
                    object.addProperty("name", editNombre.getText().toString());
                    object.addProperty("lastName", editApellido.getText().toString());
                    object.addProperty("telephone", telephone);
                    showLoading("Registrando...");
                    Ion.with(this).load(Constants.URL_UPDATE_USER_INFO)
                            .setHeader("Authorization", Utils.getAuthorizationString())
                            .setJsonObjectBody(object)
                            .asString()
                            .withResponse()
                            .setCallback(new FutureCallback<Response<String>>() {
                                @Override
                                public void onCompleted(Exception e, Response<String> result) {
                                    dismissLoading();
                                    if (result.getHeaders().code() == 200) {
                                        Toast.makeText(CambiarDatosUsuario.this, R.string.info_updated, Toast.LENGTH_SHORT).show();
                                    } else {
                                        new SweetAlertDialog(CambiarDatosUsuario.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Deliver App")
                                                .setContentText("Ocurrió un error al actualizar los datos: \n" + e.getLocalizedMessage() )
                                                .setConfirmText("Aceptar")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();

                                                    }
                                                })
                                                .show();
                                    }
                                }
                            });
                    /*
                        if (editNewPassword.getText().toString().equals(editConfirmPassword.getText().toString())) {
                            final Context context = this;
                            showLoading("Registrando...");
                            createUser(context,
                                    editPassword.getText().toString(),
                                    editNombre.getText().toString(),
                                    editApellido.getText().toString(),
                                    editNumeroTelefonoLada.getText().toString() + editNuwNumTelefono.getText().toString(),
                                    new FutureCallback<Response<String>>() {
                                        @Override
                                        public void onCompleted(Exception e, Response<String> result) {
                                            if (e == null) {
                                                if (result.getHeaders().code() == 400) { //Ya existe
                                                    showErrorMessage("El usuario proporcionado ya existe en nuestro sistema.", null);
                                             } else {
                                                    login(editEmail.getText().toString(), editPassword.getText().toString());
                                             }
                                         } else {
                                             Toast.makeText(context, result + "\n" + e.getLocalizedMessage(), Toast.LENGTH_SHORT);
                                           }
                                          }
                                   });
                             } else {
                                Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                            }
                        } */

                }
                break;

            }
            case R.id.btn_logoff:
            {
                new SweetAlertDialog(CambiarDatosUsuario.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Deliver App")
                        .setContentText(getResources().getString(R.string.log_off_question))
                        .setConfirmText("Sí")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                Context context = CambiarDatosUsuario.this;

                                DeliverApplication.getInstance().setToken(null);
                                DeliverApplication.getInstance().setUserInfoBeenRetrieved(false);
                                Utils.saveOauthInfo(context, null);
                                Utils.saveCard(context, null);
                                OauthInfo.removeOauthInfo();

                                Intent intent = new Intent();
                                intent.setClass(context, InicioSesion.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                finish();
                                context.startActivity(intent);
                            }
                        })
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        }
    }
    public void login(String userName, String password) {
        GetToken(userName, password, new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                dismissLoading();
                if (e == null) {
                    try {
                        String strResult = result.getResult();
                        JSONObject object = new JSONObject(strResult);
                        if (object.has("error")) {
                            String message = object.getString("error_description");
                            showQuickMessage(message);
                        } else {
                            OauthInfo info = new OauthInfo();
                            info.setAccessToken(object.getString("access_token"));
                            info.setRefreshToken(object.getString("refresh_token"));
                            info.setExpiresIn(object.getInt("expires_in"));
                            Utils.saveOauthInfo(CambiarDatosUsuario.this, info);
                            Intent intent = new Intent();
                            intent.setClass(CambiarDatosUsuario.this, RegistroFormaPagoTDTC.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    showQuickMessage(e.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

        }
    }
}
