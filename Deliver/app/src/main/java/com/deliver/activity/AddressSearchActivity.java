package com.deliver.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.deliver.R;
import com.deliver.customobjects.Address;
import com.deliver.customobjects.responses.GeoLocationResult;
import com.deliver.utils.Constants;
import com.deliver.utils.Utils;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AddressSearchActivity extends BaseActivity implements View.OnClickListener, TextWatcher, AdapterView.OnItemClickListener {

    private EditText addressEditText;
    private Button deleteButton;
    private ArrayList<Address> results = new ArrayList<>();
    private ListView resultsListView;
    private SearchResultsAdapter adapter;
    private String lastSearch;
    private GeoLocationResult addressSearchLocationResult;
    ArrayList<android.location.Address> addresses;
    Gson gson = new Gson();
    private Geocoder geocoder;

    GoogleSearchResultsAdapter googleAdapter;
    private ArrayList<android.location.Address> resultsGoogleAddresses = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_search);
        addressEditText = (EditText)findViewById(R.id.addressEditText);
        addressEditText.addTextChangedListener(this);

        deleteButton = (Button)findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(this);

        resultsListView = (ListView)findViewById(R.id.resultListView);
        adapter = new SearchResultsAdapter(this, R.layout.search_result_layout, results);
        googleAdapter = new GoogleSearchResultsAdapter(this, R.layout.search_result_layout, resultsGoogleAddresses);
        resultsListView.setAdapter(googleAdapter);
        resultsListView.setOnItemClickListener(this);
        Locale locale = new Locale(Locale.getDefault().getLanguage(), "MX");
        geocoder = new Geocoder(this, locale);
        showBackButton();
        setTitle(R.string.text_enter_address);
    }

    @Override
    public void onClick(View v) {
        addressEditText.setText("");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String searchCriteria = s.toString().trim().replace(" ","+");
        if (searchCriteria.length() > 0 && !searchCriteria.equals(lastSearch)) {
            lastSearch = searchCriteria;

            if (Constants.USE_NATIVE_GEOCODER) {
                new GetAddressFromLocationAsyncTask().execute(searchCriteria);
            } else {

                String url = Constants.URL_GEOCODE;
                final String address = searchCriteria;
                url = url.replace("<address>", address);
                Ion.with(this).load(url)
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                if (!(result.getResult().startsWith("{") || result.getResult().startsWith("["))) {
                                    Log.d("SearchResult", "Search: " + address + "\n\n" + result.getResult());
                                    return;
                                }
                                addressSearchLocationResult = gson.fromJson(result.getResult(), GeoLocationResult.class);
                                ;
                                if (addressSearchLocationResult != null) {
                                    results.clear();
                                    if (addressSearchLocationResult.status.equals("OK") && addressSearchLocationResult.results.size() != 0) {
                                        results.addAll(addressSearchLocationResult.results);
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        });
            }
        } else if (s.length() == 0){
            results.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String json = "";
        if (Constants.USE_NATIVE_GEOCODER) {
            android.location.Address address = resultsGoogleAddresses.get(position);
            json = gson.toJson(address);
        } else {
            Address address = results.get(position);
            json = gson.toJson(address);
        }
        Intent returnIntent = getIntent();
        returnIntent.putExtra(Constants.KEY_ADDRESS_OBJECT, json);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
        setRightExitAnimation();
    }

    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
        setRightExitAnimation();
    }

    class SearchResultsAdapter extends ArrayAdapter<Address> {

        LayoutInflater inflater;
        public SearchResultsAdapter(Context context, int resource, List<Address> objects) {
            super(context, resource, objects);
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = inflater.inflate(R.layout.search_result_layout, parent, false);
                ViewHolder holder = new ViewHolder();
                holder.nameTextView = (TextView)v.findViewById(R.id.nameTextView);
                holder.addressTextView = (TextView)v.findViewById(R.id.addressTextView);
                v.setTag(holder);
            }
            ViewHolder holder = (ViewHolder)v.getTag();
            Address address = getItem(position);
            holder.nameTextView.setText( address.getName());
            holder.addressTextView.setText(address.getDetails());

            return v;
        }
    }

    class GoogleSearchResultsAdapter extends ArrayAdapter<android.location.Address> {

        LayoutInflater inflater;
        public GoogleSearchResultsAdapter(Context context, int resource, List<android.location.Address> objects) {
            super(context, resource, objects);
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = inflater.inflate(R.layout.search_result_layout, parent, false);
                ViewHolder holder = new ViewHolder();
                holder.nameTextView = (TextView)v.findViewById(R.id.nameTextView);
                holder.addressTextView = (TextView)v.findViewById(R.id.addressTextView);
                v.setTag(holder);
            }
            ViewHolder holder = (ViewHolder)v.getTag();
            android.location.Address address = getItem(position);
            holder.nameTextView.setText( address.getFeatureName());
            holder.addressTextView.setText(Utils.getFormattedAddress(address));

            return v;
        }
    }

    private class GetAddressFromLocationAsyncTask extends AsyncTask<String, Void, List<android.location.Address>> {

        @Override
        protected List<android.location.Address> doInBackground(String... params) {
            String location = params[0];
            List<android.location.Address> addresses = null;
            try {
                addresses = geocoder.getFromLocationName(location,25);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                return addresses;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<android.location.Address> address) {
            super.onPostExecute(address);
            if (address == null)
                return;
            resultsGoogleAddresses.clear();
            resultsGoogleAddresses.addAll(address);
            googleAdapter.notifyDataSetChanged();
        }
    }


    private class ViewHolder {
        public TextView nameTextView;
        public TextView addressTextView;
    }
}
