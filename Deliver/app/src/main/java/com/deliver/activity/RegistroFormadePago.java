package com.deliver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.deliver.R;
import com.facebook.appevents.AppEventsLogger;

public class RegistroFormadePago extends BaseActivity implements View.OnClickListener {


    Button depostioBancario, depositoTarjeta, contraEntreja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_formade_pago);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        depostioBancario =(Button)findViewById(R.id.btnDepositoBancario);
        depositoTarjeta =(Button)findViewById(R.id.btnDepositoTarjeta);
        contraEntreja = (Button)findViewById(R.id.btnPagoContraEntrega);

        depositoTarjeta.setOnClickListener(this);
        depostioBancario.setOnClickListener(this);
        contraEntreja.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new  Intent();
        switch (v.getId()){

            case R.id.btnDepositoBancario:
                intent.setClass(this,RegistroFormaPagoTDTC.class);
                break;
            case R.id.btnDepositoTarjeta:
                intent.setClass(this,RegistroFormaPagoDBTE.class);
                break;
            case R.id.btnPagoContraEntrega:
                intent.setClass(this,RegistroFormaPagoCTE.class);
                break;
        }
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }
}
