package com.deliver.activity;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.deliver.R;
import com.deliver.customobjects.ServiceType;

public class ServicioLogistico extends BaseActivity implements AdapterView.OnItemClickListener {

    private ListView serviceTypeListView;
    private ServiceTypeAdapter serviceTypeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_logistico);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        serviceTypeListView = (ListView)findViewById(R.id.serviceTypeListView);
        serviceTypeAdapter = new ServiceTypeAdapter(this, R.layout.service_type_cell);
        serviceTypeListView.setAdapter(serviceTypeAdapter);
        serviceTypeListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        serviceTypeAdapter.setCurrentSelectedPosition(position);
        ServiceType type = serviceTypeAdapter.getItem(position);
        Intent intent = new Intent();
        intent.setClass(this, Transporte.class);
        intent.putExtra("Service",type);
        startActivity(intent);
    }

    private class ServiceTypeAdapter extends ArrayAdapter<ServiceType> {
        LayoutInflater inflater;
        ServiceType types[] = new ServiceType[]{ServiceType.LocalCourier, ServiceType.LocalTransport, ServiceType.OutsideTransport};
        int layoutResourceId;
        int currentSelectedPosition;
        public ServiceTypeAdapter(Context context, int resource) {
            super(context, resource);
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutResourceId = resource;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder holder;
            if (view == null) {
                view = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.imageView = (ImageView)view.findViewById(R.id.image);
                holder.textView = (TextView) view.findViewById(R.id.text);
                view.setTag(holder);
            } else {
                holder = (ViewHolder)view.getTag();
            }

            ServiceType type = types[position];
            holder.imageView.setBackgroundResource(type.getImageResource());
            holder.textView.setText(type.getStringResource());
            if (position == currentSelectedPosition) {
                view.setSelected(true);
            }
            return view;
        }

        @Override
        public ServiceType getItem(int position) {
            return types[position];
        }

        public void setCurrentSelectedPosition(int position) {
            currentSelectedPosition = position;
            notifyDataSetChanged();
        }

        public int getCount() {
            return types.length;
        }
    }



    private class ViewHolder {
        public ImageView imageView;
        public TextView textView;
    }
}
