package com.deliver.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.deliver.R;
import com.deliver.customobjects.Incident;
import com.deliver.utils.Constants;
import com.deliver.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CrearIncidencia extends BaseActivity implements View.OnClickListener {

    public static Bitmap sBitmap;
    Bitmap mBitmap;
    private ImageView imgMapSnapShot;
    private Spinner spinner;
    private Button btnCrear;
    private long serviceRequestId;
    private LatLng location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_incidencia);
        mBitmap = sBitmap;
        sBitmap = null;
        imgMapSnapShot = (ImageView)findViewById(R.id.mapSnapShot);
        imgMapSnapShot.setImageBitmap(mBitmap);
        spinner = (Spinner)findViewById(R.id.incidenceTypeSpinner);
        btnCrear = (Button)findViewById(R.id.btn_crear_incidencia);
        btnCrear.setOnClickListener(this);

        serviceRequestId = getIntent().getLongExtra("serviceRequestId", 0);
        location = (LatLng)getIntent().getParcelableExtra("location");
        hideTopBar();
    }

    public void onStart() {
        super.onStart();
        showLoading("Cargando tipo de incidencias...");
        Ion.with(this).load(Constants.URL_INCIDENT_GET_TYPES)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        dismissLoading();
                        if (e == null && result.getHeaders().code() == 200) {
                            Gson gson = new Gson();
                            ArrayList<Incident> incidentTypes = gson.fromJson(result.getResult(), new TypeToken<ArrayList<Incident>>(){}.getType());
                            CustomAdapter adapter = new CustomAdapter(CrearIncidencia.this, R.layout.incident_type_row_layout, incidentTypes);
                            spinner.setAdapter(adapter);
                        } else if (e != null) {

                        } else {

                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_crear_incidencia:
            {
                if (spinner.getSelectedItemPosition() == 0) {
                    return;
                }
                JsonObject object = new JsonObject();
                Incident incident = (Incident)spinner.getSelectedItem();
                object.addProperty("latitude", location.latitude);
                object.addProperty("longitude", location.longitude);
                object.addProperty("incidentTypeId", incident.getId());
                object.addProperty("serviceRequestId", serviceRequestId);
                showLoading("Creando incidencia...");
                Ion.with(this).load(Constants.URL_INCIDENT_CREATE)
                        .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                        .setJsonObjectBody(object)
                        .asString()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                dismissLoading();
                                if (e == null && result.getHeaders().code() == 201) {
                                    Gson gson = new Gson();
                                    JsonObject object = gson.fromJson(result.getResult(), JsonObject.class);
                                    long id = object.get("id").getAsLong();
                                    new SweetAlertDialog(CrearIncidencia.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Deliver App")
                                            .setContentText("La incidencia se creó satisfactoriamente.")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismiss();
                                                    finish();
                                                }
                                            })
                                            .show();
                                } else {

                                }
                            }
                        });
            }
                break;
        }



    }

    public class CustomAdapter extends ArrayAdapter<Incident> {
        ArrayList<Incident> objectmi;

        public CustomAdapter(Context context, int resourceId,
                             ArrayList<Incident> objects) {
            super(context, resourceId, objects);
            this.objectmi = objects;
            // TODO Auto-generated constructor stub
        }

        public int getCount() {
            return super.getCount() + 1;
        }

        public Incident getItem(int position) {
            if (position == 0) return null;
            return super.getItem(position -1);
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView,
                                  ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.incident_type_row_layout,
                        parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.incidentName);
            if (position == 0) {
                label.setText("Selecciona la incidencia");
            } else {
                label.setText(objectmi.get(position - 1).getName());
            }
            return row;
        }

    }
}
