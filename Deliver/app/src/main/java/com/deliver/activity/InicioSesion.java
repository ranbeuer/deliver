package com.deliver.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import com.deliver.DeliverApplication;
import com.deliver.R;
import com.deliver.customobjects.DeliverCard;
import com.deliver.customobjects.responses.WhoIAmResponse;
import com.deliver.services.LocationService;
import com.deliver.utils.Constants;
import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.Future;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InicioSesion extends BaseActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    Button btn_registrar, btn_FB;
    private EditText userNameEdit;
    private EditText passwordEdit;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private Button btn_iniciosesion;
    private Button btn_olvidar;
    private String firstName;
    private String lastName;
    private String email;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);


        userNameEdit = (EditText)findViewById(R.id.editText);
        passwordEdit = (EditText)findViewById(R.id.editText2);

        passwordEdit.setOnEditorActionListener(this);

        btn_registrar = (Button)findViewById(R.id.btn_registrar);
        btn_FB = (Button)findViewById(R.id.btn_FB);
        btn_iniciosesion = (Button)findViewById(R.id.btn_ingresar);
        btn_iniciosesion.setOnClickListener(this);
        btn_registrar.setOnClickListener(this);
        btn_olvidar = (Button)findViewById(R.id.btn_olvidar);
        btn_olvidar.setOnClickListener(this);
//        btn_FB.setOnClickListener(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile", "user_friends", "email", "user_mobile_phone");

        // Other app specific specialization

        // Callback registration
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    showLoading("Iniciando sesión...");
                                    email  = object.getString("email");
                                    String password = Utils.getFacebookPasswordFromEmail(email);
                                    firstName = object.getString("first_name");
                                    lastName = object.getString("last_name");
                                    login(email, password, true);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,name,email");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "On Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code\
                Log.v("LoginActivity", exception.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_ingresar:
                tryLogin();
                break;
            case R.id.btn_registrar: {
                Intent intent = new Intent();
                intent.setClass(this, RegistroUsuario.class);
                startActivity(intent);
            }
                break;
            case R.id.btn_FB:
                break;
            case R.id.btn_olvidar:
            {
                Intent intent = new Intent();
                intent.setClass(this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
                break;
        }

    }

    private void tryLogin() {
        if(passwordEdit.getText().length() != 0 && userNameEdit.getText().length() != 0) {
            showLoading("Iniciando sesión...");
            login(userNameEdit.getText().toString(), passwordEdit.getText().toString(), false);
        } else {
            String message = "Por favor introduce ";
            boolean missingUserName = false;
            if (userNameEdit.getText().length() == 0){
                message = message + "el nombre de usuario";
                missingUserName = true;
            }
            if (passwordEdit.getText().length() == 0) {
                if (missingUserName) {
                    message = message + " y ";
                }
                message = message + "la contraseña.";
            } else {
                message = message + ".";
            }
            showQuickMessage(message);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inicio_sesion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onStart() {
        super.onStart();

        OauthInfo info = OauthInfo.getInstance();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.VIBRATE}, Constants.CARD_PERMISSIONS);
        }

        if (info != null) {
            if (!info.isSessionValid()) {
                RefreshToken();
            } else {
                retrieveInfo();
            }
        }
    }

    private void moveForward() {
        DeliverCard card = DeliverApplication.getInstance().getCurrentCard();
        Intent intent = new Intent();
        if (card != null) {
            intent.setClass(this, ServicioLogistico
                    .class);
        } else {
            intent.setClass(this, RegistroFormaPagoTDTC.class);
        }
        finish();
        startActivity(intent);
    }

    private void RefreshToken() {
        showLoading("Iniciando sesión...");
        OauthInfo info = OauthInfo.getInstance();
        String bodyString = "grant_type=refresh_token&refresh_token="+ info.getRefreshToken()
                + "&client_secret=" + Constants.CLIENT_SECRET
                + "&client_id="+Constants.CLIENT_ID;
        final Context context = this;
        Future<String> future= Ion.with(this).load(Constants.URL_TOKEN)
                .setHeader("Authorization", Utils.getAuthorizationString())
                .setHeader("Content-Type", "application/x-www-form-urlencoded")
                .setStringBody(bodyString)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            try {
                                JSONObject object = new JSONObject(result);
                                OauthInfo info = new OauthInfo();
                                info.setAccessToken(object.getString("access_token"));
                                info.setRefreshToken(object.getString("refresh_token"));
                                info.setExpiresIn(object.getInt("expires_in"));
                                DeliverApplication.getInstance().setToken(object.getString("access_token"));
                                Utils.saveOauthInfo(InicioSesion.this, info);
                                retrieveInfo();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                        } else {

                        }
                    }
                });

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.CARD_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                return;
            }
        }
    }

    public void login(final String userName, final String password, final boolean isFacebook) {
        showLoading("Iniciando sesión...");
        GetToken(userName, password, new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                dismissLoading();
                if (e == null) {
                    try {
                        String strResult = result.getResult();
                        JSONObject object = new JSONObject(strResult);
                        if (object.has("error")) {
                            String message = object.getString("error_description");
                            int code = result.getHeaders().code();
                            if (message.equalsIgnoreCase("bad credentials")) {
                                if (isFacebook) { //Not registered from facebook
                                    dismissLoading();
                                    final Context context = InicioSesion.this;
                                    showLoading("Creando usuario...");
                                    Intent intent = new Intent();
                                    intent.setClass(getApplicationContext(), RegistroUsuario.class);
                                    intent.putExtra("userName", email);
                                     intent.putExtra("password", password);
                                    intent.putExtra("email", email);
                                    intent.putExtra("firstName", firstName);
                                    intent.putExtra("lastName", lastName);
                                    startActivity(intent);
                                    dismissLoading();
//                                    createUser(context, userName, password, email, firstName, lastName, "0", new FutureCallback<Response<String>>() {
//                                        @Override
//                                        public void onCompleted(Exception e, Response<String> result) {
//                                            if (e == null) {
//                                                if (result.getHeaders().code() == 400) { //Ya existe
//                                                    String strResult = result.getResult();
//                                                    try {
//                                                        JSONObject object = new JSONObject(strResult);
//                                                        if (object.has("error")) {
//                                                            String message = object.getString("error_description");
//                                                            showErrorMessage(message, new SweetAlertDialog.OnSweetClickListener() {
//                                                                @Override
//                                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
//
//                                                                }
//                                                            });
//                                                        }
//                                                    } catch (JSONException e1) {
//                                                        e1.printStackTrace();
//                                                    }
//                                                } else {
//                                                    showLoading("Iniciando sesión...");
//                                                    login(userName, password, true);
//                                                }
//                                            } else {
//                                                Toast.makeText(context, result + "\n" + e.getLocalizedMessage(), Toast.LENGTH_SHORT);
//                                            }
//                                        }
//                                    });
                                } else {
                                    showQuickMessage(R.string.bad_credentials);
                                }
                            } else {
                                showQuickMessage(message);
                            }
                        } else {
                            OauthInfo info = new OauthInfo();
                            info.setAccessToken(object.getString("access_token"));
                            info.setRefreshToken(object.getString("refresh_token"));
                            info.setExpiresIn(object.getInt("expires_in"));
                            DeliverApplication.getInstance().setToken(object.getString("access_token"));
                            Utils.saveOauthInfo(InicioSesion.this, info);
                            retrieveInfo();

                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    showQuickMessage(e.getLocalizedMessage());
                }
            }
        });
    }

    private void retrieveInfo() {
        DeliverApplication.getInstance().retrieveInformation(new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                if (e == null) {
                    Gson gson = new Gson();
                    WhoIAmResponse response = gson.fromJson(result.getResult(), WhoIAmResponse.class);
                    if (response.getError() != null) {
                        showErrorMessage(response.getErrorDescription(), new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        });
                    }
                    DeliverApplication.getInstance().processWhiIamResponse(response);
                    Log.d("Who I am Response", "" + response);
                    if (!DeliverApplication.getInstance().isUserProvider()) {
                        moveForward();
                    } else {
                        if (!isServiceRunning(LocationService.class)) {
                            startLocationService();
                            DeliverApplication.getInstance().startGettingLocations();
                        }
                        moveToMapAsProvider();
                    }
                } else {
                    showErrorMessage(e.getLocalizedMessage(), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            tryLogin();
        }
        return false;
    }
}
