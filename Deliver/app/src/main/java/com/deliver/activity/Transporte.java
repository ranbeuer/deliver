package com.deliver.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.deliver.R;
import com.deliver.customobjects.DeliverCard;
import com.deliver.customobjects.ServiceType;
import com.deliver.customobjects.TransportType;
import com.deliver.customobjects.Transportation;
import com.deliver.utils.Constants;
import com.deliver.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

public class Transporte extends BaseActivity implements AdapterView.OnItemClickListener {

    private ListView transportListView;
    private TransportTypeAdapter transportTypeAdapter;
    private ServiceType type;
    private List<Transportation> availableTransportations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transporte);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        transportListView = (ListView)findViewById(R.id.transportListView);
        transportTypeAdapter = new TransportTypeAdapter(this, R.layout.transport_type_cell);
//        transportListView.setAdapter(transportTypeAdapter);
        transportListView.setOnItemClickListener(this);

        type = (ServiceType) getIntent().getExtras().getSerializable("Service");

    }

    public void onStart() {
        super.onStart();
        getTransportationWithService(type);
    }

    private void getTransportationWithService(ServiceType type) {
        Ion.with(this)
                .load(Constants.URL_GET_TRANSPORT + type.getId())
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null) {
                            Gson gson = new Gson();
                            String json = result.getResult();
                            availableTransportations = gson.fromJson(json, new TypeToken<List<Transportation>>(){}.getType());;
                            TransportationAdapter adapter = new TransportationAdapter(Transporte.this, R.layout.transport_type_cell, availableTransportations);
                            transportListView.setAdapter(adapter);
                        } else {

                        }
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();
        Transportation transportation = ((TransportationAdapter)transportListView.getAdapter()).getItem(position);
        intent.setClass(this, CharacteristicsActivity.class);
        intent.putExtra("Transport", transportation);
        intent.putExtra("Service", type);
        startActivity(intent);
    }

    private class TransportTypeAdapter extends ArrayAdapter<TransportType> {
        LayoutInflater inflater;
        TransportType types[] = new TransportType[]{TransportType.Bicycle,TransportType.Motorcycle, TransportType.Automobile,
            TransportType.Van, TransportType.Truck};
        int layoutResourceId;
        int currentSelectedPosition;
        public TransportTypeAdapter(Context context, int resource) {
            super(context, resource);
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutResourceId = resource;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder holder;
            if (view == null) {
                view = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.imageView = (ImageView)view.findViewById(R.id.image);
                holder.textView = (TextView) view.findViewById(R.id.text);
                view.setTag(holder);
            } else {
                holder = (ViewHolder)view.getTag();
            }

            TransportType type = types[position];
            holder.imageView.setBackgroundResource(type.getImageResource());
            holder.textView.setText(type.getStringResource());
            if (position == currentSelectedPosition) {
                view.setSelected(true);
            }
            return view;
        }

        public void setCurrentSelectedPosition(int position) {
            currentSelectedPosition = position;
            notifyDataSetChanged();
        }

        public int getCount() {
            return types.length;
        }
    }

    private class TransportationAdapter extends ArrayAdapter<Transportation> {
        LayoutInflater inflater;
        List<Transportation> transportations;
        int layoutResourceId;
        int currentSelectedPosition;
        public TransportationAdapter(Context context, int resource, List<Transportation> transportations) {
            super(context, resource, transportations);
            this.transportations = transportations;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutResourceId = resource;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder holder;
            if (view == null) {
                view = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.imageView = (ImageView)view.findViewById(R.id.image);
                holder.textView = (TextView) view.findViewById(R.id.text);
                view.setTag(holder);
            } else {
                holder = (ViewHolder)view.getTag();
            }

            Transportation transportation = getItem(position);
            TransportType type = TransportType.fromStringId(transportation.id);
            holder.imageView.setBackgroundResource(type.getImageResource());
            holder.textView.setText(transportation.name);
            if (position == currentSelectedPosition) {
                view.setSelected(true);
            }
            return view;
        }

        public void setCurrentSelectedPosition(int position) {
            currentSelectedPosition = position;
            notifyDataSetChanged();
        }

        public int getCount() {
            return transportations.size();
        }
    }


    private class ViewHolder {
        public ImageView imageView;
        public TextView textView;
    }
}
