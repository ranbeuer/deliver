package com.deliver.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.deliver.R;
import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.Future;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistroUsuario extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final int TERMS_AND_CONDITIONS = 1000;
    private Button btnEnviar;
    private CheckBox checkShowPassword;
    private EditText editConfirmPassword;
    private EditText editEmail;
    private EditText editApellido;
    private EditText editNombre;
    private EditText editPassword;
    private EditText editUserName;
    private EditText editNumTelefono;
    Button btn_FormasPago, btn_FB, btn_AvisoPrivacidad;
    private CheckBox acceptConditionsCheckbox;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private EditText editNumeroTelefonoLada;
    private Button btn_TerminosYCondiciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        btnEnviar = (Button)findViewById(R.id.btn_RegistroEnviar);
        btnEnviar.setOnClickListener(this);

        checkShowPassword = (CheckBox)findViewById(R.id.checkBox);
        checkShowPassword.setOnCheckedChangeListener(this);

        editNombre = (EditText)findViewById(R.id.editTextNombre);
        editApellido = (EditText)findViewById(R.id.editTextApellido);
        editEmail = (EditText)findViewById(R.id.editEMail);
        editUserName = (EditText)findViewById(R.id.editNombreUsuario);
        editPassword = (EditText)findViewById(R.id.editPassword);
        editConfirmPassword = (EditText)findViewById(R.id.editConfirmPassword);
        editNumeroTelefonoLada = (EditText)findViewById(R.id.editNumeroTelefonoLada);
        editNumTelefono = (EditText)findViewById(R.id.editNumeroTelefono);
        btn_FormasPago = (Button) findViewById(R.id.btn_FormasPago);
        btn_FB = (Button) findViewById(R.id.btn_FB);
        btn_AvisoPrivacidad = (Button) findViewById(R.id.btn_AvisoPrivacidad);
        btn_TerminosYCondiciones = (Button)findViewById(R.id.btn_terms_and_conditions);

        btn_FormasPago.setOnClickListener(this);
        btn_FB.setOnClickListener(this);
        btn_AvisoPrivacidad.setOnClickListener(this);
        btn_TerminosYCondiciones.setOnClickListener(this);
        acceptConditionsCheckbox = (CheckBox)findViewById(R.id.termsAndConditionsCheck);
        acceptConditionsCheckbox.setEnabled(false);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Intent intent = getIntent();
        if (intent.hasExtra("userName")) {
            String userName = intent.getStringExtra("userName");
            String email = intent.getStringExtra("email");
            String password = intent.getStringExtra("password");
            String firstName = intent.getStringExtra("firstName");
            String lastName = intent.getStringExtra("lastName");

            editUserName.setText(userName);
            editUserName.setEnabled(false);
            editNombre.setText(firstName);
            editNombre.setEnabled(false);
            editApellido.setText(lastName);
            editApellido.setEnabled(false);
            editPassword.setText(password);
            editPassword.setEnabled(false);
            editConfirmPassword.setText(password);
            editConfirmPassword.setEnabled(false);
            editEmail.setText(email);
            editEmail.setEnabled(false);

        }

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {

            case R.id.btn_FormasPago:
                intent.setClass(this, RegistroFormadePago.class);
                break;
            case R.id.btn_FB:

                break;
            case R.id.btn_AvisoPrivacidad:
            {
                intent.setClass(this, ShowInfoActivity.class);
                intent.putExtra("type", ShowInfoActivity.InfoType.Privacy);
                startActivity(intent);
            }
                break;
            case R.id.btn_RegistroEnviar: {
                if (editNombre.getText().toString().isEmpty() || editApellido.getText().toString().isEmpty() || editEmail.getText().toString().isEmpty()
                        || editPassword.getText().toString().isEmpty() || editPassword.getText().toString().isEmpty()
                        || editNumTelefono.getText().toString().isEmpty() || editNumeroTelefonoLada.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Faltan datos", Toast.LENGTH_SHORT).show();
                } else if (!acceptConditionsCheckbox.isChecked()){
                    new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Deliver")
                            .setContentText("Debes aceptar los términos y condiciones para continuar")
                            .setConfirmText("Aceptar")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            });
                } else {
                    if (editPassword.getText().toString().equals(editConfirmPassword.getText().toString())) {
                        final Context context = this;
                        showLoading("Registrando...");
                        createUser(context, editEmail.getText().toString(),
                                editPassword.getText().toString(),
                                editEmail.getText().toString(),
                                editNombre.getText().toString(),
                                editApellido.getText().toString(),
                                editNumeroTelefonoLada.getText().toString() + editNumTelefono.getText().toString(),
                                new FutureCallback<Response<String>>() {
                            @Override
                            public void onCompleted(Exception e, Response<String> result) {
                                if (e == null) {
                                    if (result.getHeaders().code() == 400) { //Ya existe
                                        dismissLoading();
                                        showErrorMessage("El usuario proporcionado ya existe en nuestro sistema.", null);
                                    } else {
                                        login(editEmail.getText().toString(), editPassword.getText().toString());
                                    }
                                } else {
                                    dismissLoading();
                                    Toast.makeText(context, result + "\n" + e.getLocalizedMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        });
                    } else {
                        Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case R.id.btn_terms_and_conditions:
            {
                intent.setClass(this, ShowInfoActivity.class);
                intent.putExtra("type", ShowInfoActivity.InfoType.TermsAndConditions);
                startActivityForResult(intent, TERMS_AND_CONDITIONS);
            }
        }

    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            editConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int result, Intent data) {
        if (requestCode == TERMS_AND_CONDITIONS) {
            acceptConditionsCheckbox.setChecked(result == RESULT_OK);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void login(String userName, String password) {
        GetToken(userName, password, new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                dismissLoading();
                if (e == null) {
                    try {
                        String strResult = result.getResult();
                        JSONObject object = new JSONObject(strResult);
                        if (object.has("error")) {
                            String message = object.getString("error_description");
                            showQuickMessage(message);
                        } else {
                            OauthInfo info = new OauthInfo();
                            info.setAccessToken(object.getString("access_token"));
                            info.setRefreshToken(object.getString("refresh_token"));
                            info.setExpiresIn(object.getInt("expires_in"));
                            Utils.saveOauthInfo(RegistroUsuario.this, info);
                            Intent intent = new Intent();
                            intent.setClass(RegistroUsuario.this, RegistroFormaPagoTDTC.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    showQuickMessage(e.getLocalizedMessage());
                }
            }
        });
    }
}
