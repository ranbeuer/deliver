package com.deliver.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.deliver.R;
import com.deliver.utils.Constants;
import com.deliver.utils.Utils;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    private Button btn_buscar;
    private EditText emailEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        btn_buscar = (Button)findViewById(R.id.btn_Buscar);
        btn_buscar.setOnClickListener(this);
        emailEdit = (EditText)findViewById(R.id.editEMail);
        emailEdit.setOnEditorActionListener(this);
    }

    @Override
    public void onClick(View v) {
        tryForgotPassword();
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            tryForgotPassword();
            return true;
        }
        return false;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.back_button:
//                finish();
//                break;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_back_button, menu);
//        return true;
//    }

    private void tryForgotPassword() {
        String email = emailEdit.getText().toString();
        if (Utils.isEmailValid(email)){
            HashMap<String, String> map = new HashMap<>();
            JsonObject object = new JsonObject();
            object.addProperty("email", email);
            showLoading("Enviando...");
            Ion.with(this).load(Constants.URL_FORGOT_PASSWORD)
                    .addHeader("Content-Type", "application/json")
                    .setStringBody(object.toString())
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            dismissLoading();
                            if (e == null) {
                                if (result.getHeaders().code() == 201) {
                                    showDialogMessage("Un correo ha sido enviado a tu cuenta.", new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                            finish();
                                        }
                                    });
                                } else if (result.getHeaders().code() == 400){
                                    showErrorMessage("El correo proporcionado no existe en nuestro sistema", null);
                                }
                            } else {
                                showErrorMessage(e.getLocalizedMessage(), null);
                            }
                        }
                    });
        } else {
            showErrorMessage("Debes introducir un correo válido", null);
        }

    }
}
