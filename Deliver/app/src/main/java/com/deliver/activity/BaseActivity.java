package com.deliver.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.Toast;

import com.deliver.R;
import com.deliver.customobjects.responses.ResponseError;
import com.deliver.services.LocationService;
import com.deliver.utils.Constants;
import com.deliver.utils.OauthInfo;
import com.deliver.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.Future;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by leonardocid on 4/8/16.
 */
public class BaseActivity extends AppCompatActivity {
    private ProgressDialog pDialog;
    protected Gson gson = new Gson();

    public void showLoading(String message) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(message);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
//        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//
//            }
//        });
            pDialog.show();
        } else {
            pDialog.setMessage(message);
            pDialog.show();
        }
    }

    public void dismissLoading() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showQuickMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showQuickMessage(int messageResource) {
        Toast.makeText(this, messageResource, Toast.LENGTH_SHORT).show();
    }

    public void GetToken(String username, String password, FutureCallback<Response<String>> callback) {
        String bodyString = "username=" + username + "&password="+password+"&grant_type=password&scope=read%20write&client_secret="
                + Constants.CLIENT_SECRET+"&client_id="+Constants.CLIENT_ID;
        final Context context = this;
        Ion.with(this).load(Constants.URL_TOKEN)
                .setHeader("Authorization", Utils.getAuthorizationString())
                .setHeader("Content-Type", "application/x-www-form-urlencoded")
                .setStringBody(bodyString)
                .asString()
                .withResponse()
                .setCallback(callback);
    }

    public void createUser(Context context, String username, String password, String email, String name, String lastName, String telephone, FutureCallback<Response<String>> callback) {
        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("password", password);
        json.addProperty("email", email);
        json.addProperty("name", name);
        json.addProperty("lastName", lastName);
        json.addProperty("telephone", telephone);

        byte[] data = new byte[0];
        try {
            data = "deliver-app:password".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
        String authorizationString = "Basic "+base64;

        Ion.with(this).load(Constants.URL_NEW_USER)
                .setHeader("Authorization", authorizationString)
                .setJsonObjectBody(json)
                .asString()
                .withResponse()
                .setCallback(callback);
    }

    public void showErrorMessage(String message, SweetAlertDialog.OnSweetClickListener listener) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Deliver App - Error")
                .setContentText(message)
                .setConfirmClickListener(listener)
                .show();
    }

    public void showDialogMessage(String message, SweetAlertDialog.OnSweetClickListener listener) {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Deliver App")
                .setContentText(message)
                .setConfirmClickListener(listener)
                .show();
    }

    protected boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    protected void startLocationService() {
        Intent intent = new Intent();
        intent.setClass(this, LocationService.class);
        startService(intent);
    }

    protected void moveToMapAsProvider() {
        Intent intent = new Intent();
        intent.setClass(this, MapsActivity.class);

        intent.putExtra("provider", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);
        finish();
    }

    public boolean checkForInvalidSessionInResponse(String responseJson, boolean showErrorMessage) {
        ResponseError response = gson.fromJson(responseJson, ResponseError.class);
        if (response.error != null) {
            if (showErrorMessage) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Deliver App")
                        .setContentText("Tu sesión expiró, por favor inicia sesión de nuevo.")
                        .setConfirmText("Aceptar")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                showCleanLogin();
                            }
                        })
                        .show();
            }
            return true;
        }
        return false;
    }

    public void showCleanLogin() {
        Utils.saveOauthInfo(this, null);
        Intent intent = new Intent();
        intent.setClass(BaseActivity.this, InicioSesion.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in);
    }

    public void showBackButton() {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else if (getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setRightExitAnimation() {
        overridePendingTransition(0, R.anim.slide_out_right);
    }

    public void setLeftEnterAnimation() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.none);
    }

    public void hideTopBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        } else if (getActionBar() != null){
            getActionBar().hide();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home ) {
            finish();
            setRightExitAnimation();
            return true;
        }
        // other menu select events may be present here

        return super.onOptionsItemSelected(item);
    }
}
