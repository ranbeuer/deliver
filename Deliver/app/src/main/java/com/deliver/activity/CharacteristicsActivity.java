package com.deliver.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.deliver.R;
import com.deliver.customobjects.Transportation;
import com.deliver.customobjects.requests.QuotationCharacteristics;

public class CharacteristicsActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView quantityTextView;
    private TextView weightTextView;
    private TextView sizeHeightTextView;
    private TextView sizeLongTextView;
    private TextView sizeWidthTextView;

    private EditText descriptionEditText;
    private RadioGroup weightType;
    private RadioGroup deliveryType ;

    private int iWeightType = 0;

    int quantity = 1, weight = 1, sizeHeight = 1, sizeLong = 1, sizeWidth = 1;
    int maxQuantity, maxWeight;
    private Transportation currentTransport;
    private int iDeliveryType = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristics);

        quantityTextView = (TextView)findViewById(R.id.quantityTextView);
        weightTextView = (TextView)findViewById(R.id.weightTextView);
        sizeHeightTextView = (TextView)findViewById(R.id.sizeHeightTextView);
        sizeLongTextView = (TextView)findViewById(R.id.sizeLongTextView);
        sizeWidthTextView = (TextView)findViewById(R.id.sizeWidthTextView);
        currentTransport = getIntent().getExtras().getParcelable("Transport");
        descriptionEditText = (EditText)findViewById(R.id.description);
        weightType = (RadioGroup)findViewById(R.id.deliveryWeightType);
        weightType.setOnCheckedChangeListener(this);
        deliveryType = (RadioGroup)findViewById(R.id.deliveryType);
        deliveryType.setOnCheckedChangeListener(this);
        if (currentTransport.maxQuantity == null)
            maxQuantity = -1;
        else
            maxQuantity = (int)Float.parseFloat(currentTransport.maxQuantity);
        maxWeight = (int)Float.parseFloat(currentTransport.maxWeight);
        updateUI();
    }

    private void updateUI() {
        quantityTextView.setText("" + quantity);
        weightTextView.setText("" + weight);
        sizeHeightTextView.setText("" + sizeHeight);
        sizeLongTextView.setText("" + sizeLong);
        sizeWidthTextView.setText("" + sizeWidth);
    }

    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.sizeHeightIncrement:
            case R.id.sizeHeightDecrement: {
                sizeHeight += (id == R.id.sizeHeightIncrement ? 1 : -1);
                if (sizeHeight < 1)
                    sizeHeight = 1;
            }
            break;

            case R.id.sizeLongIncrement:
            case R.id.sizeLongDecrement: {
                sizeLong += (id == R.id.sizeLongIncrement ? 1 : -1);
                if (sizeLong < 1)
                    sizeLong = 1;
            }
            break;

            case R.id.sizeWidthIncrement:
            case R.id.sizeWidthDecrement: {
                sizeWidth += (id == R.id.sizeWidthIncrement ? 1 : -1);
                if (sizeWidth < 1)
                    sizeWidth = 1;
            }
            break;

            case R.id.quantityIncrement:
            case R.id.quantityDecrement: {
                quantity += (id == R.id.quantityIncrement ? 1 : -1);
                if (quantity < 1)
                    quantity = 1;
                if (quantity > maxQuantity && maxQuantity > -1)
                    quantity = maxQuantity;
            }
            break;

            case R.id.weightIncrement:
            case R.id.weightDecrement: {
                weight += (id == R.id.weightIncrement ? 1 : -1);
                if (weight < 1)
                    weight = 1;
                if (weight > maxWeight)
                    weight = maxWeight;
            }
            break;
            case R.id.continueButton: {
                if (!validateFields()) return;
                Intent intent = new Intent();
                intent.setClass(this, MapsActivity.class);
                QuotationCharacteristics chars = new QuotationCharacteristics();
                chars.weight = weight * (iWeightType == 1 ? 1 : 1000);
                chars.quantity = quantity;
                chars.volumeWidth = sizeWidth;
                chars.volumeHeight = sizeLong;
                chars.volumeDepth = sizeHeight;
                chars.description = descriptionEditText.getText().toString();
                chars.transport = Long.parseLong(currentTransport.id);
                intent.putExtra("ProviderMapObject", false);
                intent.putExtra("QuotationCharacteristics", chars);

                startActivity(intent);
                finish();
                return;
            }
            case R.id.btn_restrictions:
            {
                Intent intent = new Intent();
                intent.setClass(this, ShowInfoActivity.class);
                intent.putExtra("type", ShowInfoActivity.InfoType.Restrictions);
                startActivity(intent);
            }
                break;
        }
        updateUI();
    }

    private boolean validateFields() {
        if (iWeightType == 0) {
            Toast.makeText(this, "Falta elegir el tipo de peso", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (iDeliveryType == 0) {
            Toast.makeText(this, "Falta elegir el tipo de entrega", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (descriptionEditText.getText().length() == 0) {
            Toast.makeText(this, "Debes introducir una descripción para el servicio", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch(checkedId) {
            case R.id.weightKg:
                iWeightType = 1;
                break;
            case R.id.weightTon:
                iWeightType = 2;
                break;
            case R.id.radioButtonEnvelope:
                iDeliveryType = 1;
                break;
            case R.id.radioButtonPackage:
                iDeliveryType = 2;
                break;
            case R.id.radioButtonOther:
                iDeliveryType = 3;
                break;
        }
    }
}
