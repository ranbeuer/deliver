package com.deliver.interfaces;

import android.net.Uri;

/**
 * Created by leonardocid on 9/13/16.
 */
public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
}