package com.deliver;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.alirezaafkar.json.requester.Requester;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deliver.customobjects.DeliverCard;
import com.deliver.customobjects.responses.WhoIAmResponse;
import com.deliver.customobjects.requests.LocationUpdate;
import com.deliver.utils.Constants;
import com.deliver.utils.Utils;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.ubertesters.common.models.ActivationMode;
import com.ubertesters.common.models.LockingMode;
import com.ubertesters.sdk.Ubertesters;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by leonardocid on 3/4/16.
 */
public class DeliverApplication extends Application {

    private static DeliverApplication instance;
    private String token;
    private boolean userProvider;
    private boolean hasUserInfoBeenRetrieved;
    private boolean userInfoBeenRetrieved;
    private int providerId;
    final Gson gson = new Gson();
    long lastTimeLocationupdated = 0;
    private DeliverCard currentCard;
    private long currentServiceId;

    public static DeliverApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        Map<String, String> header = new HashMap<>();
        header.put("charset", "utf-8");

        Requester.Config config = new Requester.Config(getApplicationContext());
        config.setHeader(header);
        Requester.init(config);
        FacebookSdk.sdkInitialize(getApplicationContext());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AvenirNext.ttc")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        hasUserInfoBeenRetrieved = Utils.userInfoRetrieved(getApplicationContext());
        List<DeliverCard> cards = Utils.readCards(this);
        if (cards != null && cards.size() != 0) {
            currentCard = cards.get(0);
        }
        currentServiceId = Utils.readCurrentServiceId(this);
        if (Constants.IS_DEV) {
            Ubertesters.initialize(this, LockingMode.DisableUbertesters, ActivationMode.Manual);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void retrieveInformation(FutureCallback<Response<String>> callback) {
        if (hasUserInfoBeenRetrieved()) {
            return;
        }
        setUserInfoBeenRetrieved(true);
        final Gson gson = new Gson();
        final Context context = this;
        Ion.with(this).load(Constants.URL_WHOIAM)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .asString()
                .withResponse()
                .setCallback(callback);
    }

    private boolean hasUserInfoBeenRetrieved() {
        return userInfoBeenRetrieved;
    }

    public void updateMyLocation(Location location, int providerId) {

        long now = System.currentTimeMillis();

        if (now - lastTimeLocationupdated < 10000) return;
        lastTimeLocationupdated = now;
        final Context context = this;
        LocationUpdate update = new LocationUpdate();
        update.id = providerId;
        update.latitude = location.getLatitude();
        update.longitude = location.getLongitude();

        String json = gson.toJson(update);

        Log.w("setLocation", "Request: " + json);

        Ion.with(this).load(Constants.URL_SET_LOCATION)
                .addHeader("Authorization", Utils.getLoggedAuthorizationString())
                .addHeader("Content-Type", "application/json")
                .setStringBody(json)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e == null) {
                            Log.w("setLocation", "Response: " + result.getResult());
                            result.getHeaders().code();
                        } else {
                        }
                    }
                });
    }

    public boolean isUserProvider() {
        return userProvider;
    }

    public void setUserInfoBeenRetrieved(boolean userInfoBeenRetrieved) {
        this.userInfoBeenRetrieved = userInfoBeenRetrieved;
        Utils.saveUserInfoRetrieved(getApplicationContext(), userInfoBeenRetrieved);
    }

    public void processWhiIamResponse(WhoIAmResponse response) {
        if (response.providerId != 0) {
            userProvider = true;
            providerId = response.providerId;
        }
        if (response.cardNumber != null) {
            if (getCurrentCard() == null) {
                DeliverCard card = new DeliverCard();
                card.cardNumber = response.cardNumber;
                card.token = "";
                Utils.saveCard(this, card);
                currentCard = card;
            } else if (!getCurrentCard().cardNumber.equals(response.cardNumber)){
                DeliverCard card = new DeliverCard();
                card.cardNumber = response.cardNumber;
                card.token = "";
                Utils.saveCard(this, card);
                currentCard = card;
            }
        }
    }

    public int getProviderId() {
        return providerId;
    }

    public void startGettingLocations() {

    }

    public DeliverCard getCurrentCard() {
        return currentCard;
    }

    public void setCurrentServiceId(long currentServiceId) {
        this.currentServiceId = currentServiceId;
        Utils.saveCurrentServiceId(this, currentServiceId);
    }

    public long getCurrentServiceId() {
        return currentServiceId;
    }
}
