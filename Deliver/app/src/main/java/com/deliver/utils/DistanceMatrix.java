package com.deliver.utils;

import java.util.List;

/**
 * Created by leonardocid on 6/4/16.
 */
public class DistanceMatrix {
    public List<String> destination_addresses;
    public List<String> origin_addresses;

    public List<Rows> rows;

    public class Rows {
        public List<Element> elements;
    }

    public class Element {
        public Value distance;
        public Value duration;
        public String status;
    }

    public class Value {
        public String text;
        public long value;
    }
}
