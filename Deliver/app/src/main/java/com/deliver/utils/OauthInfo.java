package com.deliver.utils;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.deliver.DeliverApplication;

/**
 * Created by leonardocid on 4/5/16.
 */
public class OauthInfo implements Parcelable{
    private String accessToken = null;
    private String refreshToken = null;
    private int expiresIn = 0;

    private static OauthInfo instance;
    private long lastUsed;

    public OauthInfo(Parcel in) {
        accessToken = in.readString();
        refreshToken = in.readString();
        expiresIn = in.readInt();
    }

    public static final Creator<OauthInfo> CREATOR = new Creator<OauthInfo>() {
        @Override
        public OauthInfo createFromParcel(Parcel in) {
            return new OauthInfo(in);
        }

        @Override
        public OauthInfo[] newArray(int size) {
            return new OauthInfo[size];
        }
    };

    public OauthInfo() {
        if (instance == null)
            instance = this;
    }

    public static OauthInfo getInstance() {
        if (instance == null) {
            DeliverApplication.getInstance().getApplicationContext();
        }
        return instance;
    }

    public static void removeOauthInfo() {
        instance = null;
    }

    public static void setInstance(OauthInfo instance) {
        OauthInfo.instance = instance;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        setLastUsed(System.currentTimeMillis());
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public void save(Context context) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accessToken);
        dest.writeString(refreshToken);
        dest.writeInt(expiresIn);
    }

    public void setLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
    }

    public long getLastUsed() {
        return lastUsed;
    }

    public boolean isSessionValid() {
        long now = System.currentTimeMillis();
        long difference = (now - lastUsed) / 1000;
        return  difference < expiresIn;
    }
}
