package com.deliver.utils;

/**
 * Created by leonardocid on 4/4/16.
 */
public class Constants {

    public final static boolean IS_DEV = false;
    public final static String BASE_URL_PROD = "http://api.deliverapp.com.mx:8080/";
    public final static String BASE_URL_DEV = "http://52.24.146.212:8080/";
    public final static String BASE_URL = IS_DEV ? BASE_URL_DEV : BASE_URL_PROD;


    public final static String CONEKTA_API_KEY_DEV = "key_I4XADuDthVh7pN9vbSxfgDg";
    public final static String CONEKTA_API_KEY_PROD = "key_CpGjzjvsB4gyL4Az4UCxieQ";
    public final static String CONEKTA_API_KEY = IS_DEV ? CONEKTA_API_KEY_DEV : CONEKTA_API_KEY_PROD;



    public final static String URL_CREATE_QUOTATION =   BASE_URL + "quotation/create";
    public final static String URL_NEW_USER =           BASE_URL + "user/create";
    public final static String URL_TOKEN =              BASE_URL + "oauth/token";
    public final static String URL_WHOIAM =             BASE_URL + "user/whoiam";
    public final static String URL_SET_LOCATION =       BASE_URL + "provider/setLocation";
    public final static String URL_FORGOT_PASSWORD =    BASE_URL + "passwordRecovery/create";
    public final static String URL_FIND_ALL_NEARBY =    BASE_URL + "provider/findAllNearby";
    public final static String URL_CREATE_CARD =        BASE_URL + "card/create";
    public final static String URL_GET_TRANSPORT =      BASE_URL + "transport/list?id=";

    public final static String URL_SERVICE_CREATE =     BASE_URL + "serviceRequest/create";
    public final static String URL_SERVICE_UPDATE =     BASE_URL + "serviceRequest/update";
    public final static String URL_SERVICE_GET =        BASE_URL + "serviceRequest/get";
    public final static String URL_SERVICE_FIND_BY_QUOTATION = BASE_URL + "serviceRequest/findByQuotation/";

    public final static String URL_PROVIDER_SERVICE_GET = BASE_URL + "providerRequest/get";

    public final static String URL_PROVIDER_REQUEST_CREATE =             BASE_URL + "providerRequest/create";
    public final static String URL_PROVIDER_REQUEST_SERVICE_EXISTS =     BASE_URL + "providerRequest/serviceExists";
    public final static String URL_PROVIDER_REQUEST_UPDATE =             BASE_URL + "providerRequest/update";
    public final static String URL_PROVIDER_REQUEST_STATUS =             BASE_URL + "providerRequest/get";
    public final static String URL_CLIENT_SERVICE_EVALUATION =           BASE_URL + "serviceEvaluation/create";

    public static final String URL_INCIDENT_GET_TYPES =                  BASE_URL + "incident/listTypes";
    public static final String URL_INCIDENT_CREATE =                     BASE_URL + "incident/create";

    public final static String URL_UPDATE_USER_INFO =                    BASE_URL + "user/update";


    public static final String PREFS_KEY_OAUTH = "PrefsOauth";    public static final String PREFS_NAME = "DeliverAppPrefs";

    public static final String PREFS_KEY_CARDS = "PrefsCards";
    public static final String CLIENT_SECRET = "w6AUbsaJXBx2m5R9D6bEdbdp4VzPfc";
    public static final Object CLIENT_ID = "deliver-app";

//    public static final String GOOGLE_API_KEY = "AIzaSyDR-WPuwzWIDn3SaOG5CLmCWAbc2JT0oPA";
    public static final String GOOGLE_API_KEY_DEV =     "AIzaSyCYSMVPnbA7nTmIBXKyBA7knfzj4KMKvjg";
    public static final String GOOGLE_API_KEY_PROD =    "AIzaSyBF2lX8O8uLRj73DV991F-Ca3mnjRWEcsw";
    public static final String GOOGLE_API_KEY = GOOGLE_API_KEY_PROD;

    public static final String GOOGLE_DISTANCE_MATRIX_API_KEY = "AIzaSyAhfSpy7AbFmniLctUqmziuiLnia2_V53Q";
    public static final String URL_REVERSE_GEOCODE="https://maps.googleapis.com/maps/api/geocode/json?latlng=<lat>,<long>&key="+ GOOGLE_API_KEY + "&sensor=false";
//    public static final String URL_REVERSE_GEOCODE="https://maps.googleapis.com/maps/api/geocode/json?latlng=<lat>,<long>&key=AIzaSyDR-WPuwzWIDn3SaOG5CLmCWAbc2JT0oPA&sensor=false";
    public static final String URL_GEOCODE="https://maps.googleapis.com/maps/api/geocode/json?address=<address>&components=administrative_area:Mexico|country:MX&key="+ GOOGLE_API_KEY + "&sensor=false";
    public static final String URL_DISTANCE_MATRIX="https://maps.googleapis.com/maps/api/distancematrix/json?origins=<sLat>,<sLon>&destinations=<dLat>,<dLon>&language=es-MX&key="+ GOOGLE_DISTANCE_MATRIX_API_KEY + "&sensor=true";
    public static final int CARD_PERMISSIONS = 50;
    public static final boolean USE_UBERTESTERS = IS_DEV;
    public static final String KEY_ADDRESS_OBJECT = "address";
    public static final String PREFS_USER_INFO_RETRIEVED = "userInfo";

    public static final Boolean USE_NATIVE_GEOCODER = true;
    public static final String PREFS_KEY_SERVICE_ID = "serviceId";
    public static final int CHANGE_CARD_REQUEST_CODE = 100;

}
