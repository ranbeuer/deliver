package com.deliver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.util.Base64;

import com.deliver.DeliverApplication;
import com.deliver.customobjects.DeliverCard;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardocid on 4/5/16.
 */
public class Utils {

    public static String getAuthorizationString() {
        byte[] data = new byte[0];
        try {
            String auth = "deliver-app:" + Constants.CLIENT_SECRET;
            data = auth.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
        String authorizationString = "Basic "+base64;
        return authorizationString;
    }

    public static String getLoggedAuthorizationString() {
        byte[] data = new byte[0];
        String auth = "";
        try {
            auth = "Bearer " + DeliverApplication.getInstance().getToken();
            data = auth.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
        String authorizationString = auth;//"Basic "+base64;
        return authorizationString;
    }


    public static OauthInfo readOauthInfo(Context context) {
        Gson gson = new Gson();
        OauthInfo info = null;
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        String restoredText = prefs.getString(Constants.PREFS_KEY_OAUTH, null);
        String json = prefs.getString(Constants.PREFS_KEY_OAUTH, null);
        if (json != null) {
            info = gson.fromJson(json, OauthInfo.class);
        }
        return info;
    }

    public static void saveOauthInfo(Context context, OauthInfo info) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(info);
        editor.putString(Constants.PREFS_KEY_OAUTH, json);
        editor.commit();
    }

    public static void saveCard(Context context, DeliverCard card) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE).edit();
        if (card != null) {
            Gson gson = new Gson();
            String cardsAsString = prefs.getString(Constants.PREFS_KEY_CARDS, null);
            List<DeliverCard> cards = null;
//        if (cardsAsString == null) {
            cards = new ArrayList<DeliverCard>();
//        } else {
//            cards = gson.fromJson(cardsAsString, new TypeToken<ArrayList<DeliverCard>>(){}.getType());
//        }
            cards.add(card);
            String json = gson.toJson(cards);
            editor.putString(Constants.PREFS_KEY_CARDS, json);
        } else {
            editor.putString(Constants.PREFS_KEY_CARDS, null);
        }
        editor.commit();
    }

    public static List<DeliverCard> readCards(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String cardsAsString = prefs.getString(Constants.PREFS_KEY_CARDS, null);
        List<DeliverCard> cards = null;
        if (cardsAsString != null) {
            cards = gson.fromJson(cardsAsString, new TypeToken<ArrayList<DeliverCard>>(){}.getType());
        }
        return cards;
    }

    public static boolean isEmailValid(String emailCandidate) {
        if (emailCandidate == null || emailCandidate.isEmpty()) return false;
        return android.util.Patterns.EMAIL_ADDRESS.matcher(emailCandidate).matches();
    }

    public static String getFacebookPasswordFromEmail(String email) {
        String result = null;
        String base64 = Base64.encodeToString(email.getBytes(), Base64.NO_WRAP);
        try {
            result = getHashRepresentation(base64);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getHashRepresentation(String string) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.reset();

        byte[] byteData = digest.digest(string.getBytes("UTF-8"));
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < byteData.length; i++){
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static boolean userInfoRetrieved(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        boolean info = prefs.getBoolean(Constants.PREFS_USER_INFO_RETRIEVED, false);
        return info;
    }

    public static void saveUserInfoRetrieved(Context context, boolean userInfo) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(Constants.PREFS_USER_INFO_RETRIEVED, userInfo);
        editor.apply();
    }

    public static String getFormattedAddress(Address address) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
            builder.append(address.getAddressLine(i));
            if (i < address.getMaxAddressLineIndex() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public static void saveCurrentServiceId(Context context, long currentServiceId) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putLong(Constants.PREFS_KEY_SERVICE_ID, currentServiceId);
        editor.apply();
    }

    public static long readCurrentServiceId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        long info = prefs.getLong(Constants.PREFS_KEY_SERVICE_ID, 0);
        return info;
    }
}
