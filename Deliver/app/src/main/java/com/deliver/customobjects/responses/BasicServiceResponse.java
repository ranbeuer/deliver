package com.deliver.customobjects.responses;

/**
 * Created by leonardocid on 8/22/16.
 */
public class BasicServiceResponse {
    private String error;
    private String error_description;

    public String getErrorDescription() {
        return error_description;
    }

    public String getError() {
        return error;
    }
}
