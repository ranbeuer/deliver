package com.deliver.customobjects;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by leonardocid on 6/4/16.
 */
public class Address {
    public String details;
    public List<AddressComponents> address_components;
    public String formatted_address;
    public Geometry geometry;
    public String place_id;
    public List<String> types;
    private int whereToStart;
    private String title;

    public String getName() {
        if (title == null) {
            AddressComponents component = address_components.get(0);
            if (component.types.get(0).contains("number")) {
                title = address_components.get(1).long_name + " " + address_components.get(0).long_name;
            } else {
                title = address_components.get(0).long_name;
                ;
            }
        }
        return title;
    }

    public String getDetails() {
        if (details == null && address_components.size() > 1) {
            StringBuilder builder = new StringBuilder();
            for (int i = 1; i < address_components.size(); i++) {
                builder.append(address_components.get(i).long_name);
                if (i < address_components.size() - 1) {
                    builder.append(", ");
                }
            }
            details = builder.toString();
        } else if (address_components.size() == 1){
            details = "";
        }
        return details;
    }

    public class AddressComponents {
        public String long_name;
        public String short_name;
        public List<String> types;
    }

    public class Geometry {
        public Location location;
        public String location_type;
        public ViewPort viewport;
    }

    public class Location {
        public double lat;
        public double lng;
        public LatLng getLatLng() {
            return new LatLng(lat, lng);
        }
    }

    public class ViewPort {
        public Location northeast;
        public Location southwest;
    }
}
