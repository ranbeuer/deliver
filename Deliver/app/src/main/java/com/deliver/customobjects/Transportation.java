package com.deliver.customobjects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by leonardocid on 5/21/16.
 */
public class Transportation implements Parcelable{
    public LogisticService service;
    public String name;
    public String id;
    public String maxWeight;
    public String maxQuantity;
    public String amount;
    public String maxDistance;
    public String amountFactor;

    protected Transportation(Parcel in) {
        name = in.readString();
        id = in.readString();
        maxWeight = in.readString();
        maxQuantity = in.readString();
        amount = in.readString();
        maxDistance = in.readString();
        amountFactor = in.readString();
    }

    public static final Creator<Transportation> CREATOR = new Creator<Transportation>() {
        @Override
        public Transportation createFromParcel(Parcel in) {
            return new Transportation(in);
        }

        @Override
        public Transportation[] newArray(int size) {
            return new Transportation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(maxWeight);
        dest.writeString(maxQuantity);
        dest.writeString(amount);
        dest.writeString(maxDistance);
        dest.writeString(amountFactor);
    }
}
