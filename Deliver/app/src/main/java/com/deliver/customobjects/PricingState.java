package com.deliver.customobjects;

/**
 * Created by leonardocid on 6/4/16.
 */
public enum PricingState {
    StartLocation,
    DestinationLocation,
    Pricing,
    ShowingPricing,
    NoState
}
