package com.deliver.customobjects.requests;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by leonardocid on 6/4/16.
 */
public class QuotationCharacteristics implements Parcelable {
    public int weight;
    public int quantity;
    public float volumeWidth;
    public float volumeHeight;
    public float volumeDepth;
    public long transport;
    public String description;
    public double sourceLatitude;
    public double sourceLongitude;
    public double destinationLatitude;
    public double destinationLongitude;
    public double distance;

    public QuotationCharacteristics() {
        description = "";
    }

    protected QuotationCharacteristics(Parcel in) {
        weight = in.readInt();
        quantity = in.readInt();
        volumeWidth = in.readFloat();
        volumeHeight = in.readFloat();
        volumeDepth = in.readFloat();
        transport = in.readLong();
        description = in.readString();
        sourceLatitude = in.readDouble();
        sourceLongitude = in.readDouble();
        destinationLatitude = in.readDouble();
        destinationLongitude = in.readDouble();
        distance = in.readDouble();
    }

    public static final Creator<QuotationCharacteristics> CREATOR = new Creator<QuotationCharacteristics>() {
        @Override
        public QuotationCharacteristics createFromParcel(Parcel in) {
            return new QuotationCharacteristics(in);
        }

        @Override
        public QuotationCharacteristics[] newArray(int size) {
            return new QuotationCharacteristics[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(weight);
        dest.writeInt(quantity);
        dest.writeFloat(volumeWidth);
        dest.writeFloat(volumeHeight);
        dest.writeFloat(volumeDepth);
        dest.writeLong(transport);
        dest.writeString(description);
        dest.writeDouble(sourceLatitude);
        dest.writeDouble(sourceLongitude);
        dest.writeDouble(destinationLatitude);
        dest.writeDouble(destinationLongitude);
        dest.writeDouble(distance);
    }
}