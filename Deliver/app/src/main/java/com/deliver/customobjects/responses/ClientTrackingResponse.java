package com.deliver.customobjects.responses;

import com.deliver.customobjects.ProviderMapObject;

/**
 * Created by leonardocid on 8/22/16.
 */
public class ClientTrackingResponse extends BasicServiceResponse {
    private int status;
    private long dateCreated;
    private long id;
    private float longitude;
    private float latitude;
    private long lastUpdated;
    private ProviderMapObject provider;

    public int getStatus() {
        return status;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public long getId() {
        return id;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public ProviderMapObject getProvider() {
        return provider;
    }
}
