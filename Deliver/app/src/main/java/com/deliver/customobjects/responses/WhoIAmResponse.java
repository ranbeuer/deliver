package com.deliver.customobjects.responses;

/**
 * Created by leonardocid on 7/1/16.
 */
public class WhoIAmResponse extends BasicServiceResponse{
    public String username;
    public String authorities[];
    public int providerId;
    public String cardNumber;
    public long currentServiceId;
}
