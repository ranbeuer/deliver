package com.deliver.customobjects;

/**
 * Created by leonardocid on 7/19/16.
 */
public class Quotation {
    private long id;
    private long transportId;
    private double amount;


    private float volumeDepth;
    private float volumeHeight;
    private float volumeWidth;
    private float weight;
    private int quantity;

    private long dateCreated;
    private String description;

    private float distance;

    private double sourceLatitude;
    private double sourceLongitude;

    private double destinationLatitude;
    private double destinationLongitude;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTransportId() {
        return transportId;
    }

    public void setTransportId(long transportId) {
        this.transportId = transportId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public float getVolumeDepth() {
        return volumeDepth;
    }

    public void setVolumeDepth(float volumeDepth) {
        this.volumeDepth = volumeDepth;
    }

    public float getVolumeHeight() {
        return volumeHeight;
    }

    public void setVolumeHeight(float volumeHeight) {
        this.volumeHeight = volumeHeight;
    }

    public float getVolumeWidth() {
        return volumeWidth;
    }

    public void setVolumeWidth(float volumeWidth) {
        this.volumeWidth = volumeWidth;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public double getSourceLatitude() {
        return sourceLatitude;
    }

    public void setSourceLatitude(double sourceLatitude) {
        this.sourceLatitude = sourceLatitude;
    }

    public double getSourceLongitude() {
        return sourceLongitude;
    }

    public void setSourceLongitude(double sourceLongitude) {
        this.sourceLongitude = sourceLongitude;
    }

    public double getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(double destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public double getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(double destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }
}
