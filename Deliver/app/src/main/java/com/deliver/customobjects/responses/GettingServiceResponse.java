package com.deliver.customobjects.responses;

import com.deliver.customobjects.ProviderMapObject;

/**
 * Created by leonardocid on 8/18/16.
 */
public class GettingServiceResponse extends BasicServiceResponse{
    private long id;
    private long dateCreated;
    private int status;
    private long lastUpdated;
    private double latitude;
    private double longitude;
    private ProviderMapObject provider;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ProviderMapObject getProvider() {
        return provider;
    }

    public void setProvider(ProviderMapObject provider) {
        this.provider = provider;
    }

}
