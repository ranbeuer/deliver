package com.deliver.customobjects.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by leonardocid on 8/1/16.
 */
public class ResponseError {
    public String error;
    @SerializedName("error_description")
    public String errorDescription;
}
