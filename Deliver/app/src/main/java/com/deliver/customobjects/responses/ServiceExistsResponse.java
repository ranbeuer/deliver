package com.deliver.customobjects.responses;

import com.deliver.customobjects.Quotation;

/**
 * Created by leonardocid on 7/20/16.
 */
public class ServiceExistsResponse extends BasicServiceResponse {
    private Quotation quotation;
    private int status;
    private long dateCreated;
    private long lastUpdated;
    private long id;

    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
