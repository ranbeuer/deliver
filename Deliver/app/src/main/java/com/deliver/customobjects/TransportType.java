package com.deliver.customobjects;

import com.deliver.R;

/**
 * Created by leonardocid on 5/7/16.
 */
public enum TransportType {
    Bicycle,
    Motorcycle,
    Automobile,
    Van,
    Truck;

    public int getStringResource() {
        switch(this) {
            case Bicycle:
                return R.string.buttonBicicleta;
            case Motorcycle:
                return R.string.buttonMotocicleta;
            case Automobile:
                return R.string.buttonAutomovil;
            case Van:
                return R.string.buttonVan;
        }
        return R.string.buttonCamioneta;
    }

    public int getImageResource() {
        switch(this) {
            case Bicycle:
                return R.drawable.selector_transport_bicicleta;
            case Motorcycle:
                return R.drawable.selector_transport_motocicleta;
            case Automobile:
                return R.drawable.selector_transport_auto;
            case Van:
                return R.drawable.selector_transport_van;
        }
        return R.drawable.selector_transport_camioneta;
    }

    public static TransportType fromStringId(String id) {
        Integer transportId = Integer.parseInt(id);
        if (transportId == 1) {
            return Bicycle;
        } else if (transportId == 2) {
            return Motorcycle;
        } else if (transportId == 3) {
            return Automobile;
        } else if (transportId == 4 || transportId == 6) {
            return Van;
        } else {
            return Truck;
        }
    }
}
