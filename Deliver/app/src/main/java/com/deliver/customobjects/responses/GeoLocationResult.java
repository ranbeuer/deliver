package com.deliver.customobjects.responses;

import com.deliver.customobjects.Address;

import java.util.List;

/**
 * Created by leonardocid on 6/18/16.
 */
public class GeoLocationResult {
    public List<Address> results;
    public String status;
}
