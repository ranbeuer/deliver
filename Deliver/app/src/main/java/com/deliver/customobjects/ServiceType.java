package com.deliver.customobjects;

import com.deliver.R;

/**
 * Created by leonardocid on 5/7/16.
 */
public enum ServiceType {
    LocalCourier,
    LocalTransport,
    OutsideTransport;

    public int getStringResource() {
        switch(this) {
            case LocalCourier:
                return R.string.buttonPaqueteria;
            case LocalTransport:
                return R.string.buttonFleteLocal;
            case OutsideTransport:
                return R.string.buttonFleteForaneo;
        }
        return 0;
    }

    public int getImageResource() {
        switch(this) {
            case LocalCourier:
                return R.drawable.selector_mensajeria;
            case LocalTransport:
                return R.drawable.selector_flete;
            case OutsideTransport:
                return R.drawable.selector_flete_foraneo;
        }
        return 0;
    }

    public String getId() {
        switch(this) {
            case LocalCourier:
                return "1";
            case LocalTransport:
                return "2";
            case OutsideTransport:
                return "3";
        }
        return "0";
    }
}
