package com.deliver.customobjects.requests;

/**
 * Created by leonardocid on 7/1/16.
 */
public class LocationUpdate {
    public int id;
    public double latitude;
    public double longitude;
}
